// create the module
var mainModule = angular.module('mainModule', ['ngRoute']);


		//////////// Route Provider///////////
		//////////////////////////////////////
		mainModule.config(function($routeProvider) {
		
			$routeProvider
			
			// route for the home page
            .when('/home', {
                templateUrl : 'pages/home.html',
                controller  : 'homeController'
            })

            // route for the about page
            .when('/about', {
                templateUrl : 'pages/about.html',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'pages/contact.html',
                controller  : 'contactController'
            })
            
            // default route
            .otherwise({
            	templateUrl : 'pages/login.html',
                controller  : 'loginController'
            });
			
		});
		
		
		
		
		
		//////// Directive////////////////////
		/////////////////////////////////////
		mainModule.directive('fileModel', ['$parse', function ($parse) {
		    return {
		        restrict: 'A',
		        link: function(scope, element, attrs) {
		            var model = $parse(attrs.fileModel);
		            var modelSetter = model.assign;
		            
		            element.bind('change', function(){
		                scope.$apply(function(){
		                    modelSetter(scope, element[0].files[0]);
		                });
		            });
		        }
		    };
		}]);
		
		
		
		
		
		/////////////Service//////////////////////////
		/////////////////////////////////////////////
		mainModule.service('fileUpload', ['$http', function ($http) {
			
		    this.uploadFileToUrl = function(file, uploadUrl){
		        var fd = new FormData();
		        fd.append('file', file);
		        $http.post(uploadUrl, fd, {
		            transformRequest: angular.identity,
		            headers: {'Content-Type': undefined}
		        })
		        .success(function(){
		        })
		        .error(function(){
		        });
		    }
		}]);
		

		
		
		
		///////////// MAIN CONTROLLER ///////////////////////////
		// create the controller and inject $scope and fileUpload service
		mainModule.controller('mainController', ['$scope', 'fileUpload', function($scope, fileUpload){
		    
		    $scope.uploadFile = function(){
		        var file = $scope.myFile;
		        console.log('file is ' );
		        console.dir(file);
		        var uploadUrl = "/fileUpload";
		        fileUpload.uploadFileToUrl(file, uploadUrl);
		    };
		    
		}]);
    
		mainModule.controller('homeController', function($scope) {
	        $scope.message = 'home page';
	    });
		
		mainModule.controller('aboutController', function($scope) {
	        $scope.message = 'about page';
	    });

	    mainModule.controller('contactController', function($scope) {
	        $scope.message = 'contact us';
	    });
		
	    mainModule.controller('loginController', function($scope) {
	        $scope.message = 'login page';
	    });
		
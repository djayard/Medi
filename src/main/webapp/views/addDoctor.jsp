<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Doctor</title>
</head>
<body>

<%@ include file="header.jsp" %>
<c:if test="${not approvedDocs.isEmpty() }">
<table>
	<tbody>
		<tr><th>Name</th><th>Specialty</th></tr>
		<c:forEach items="${approvedDocs}" var="doc" >
			<tr><td>Dr. <c:out value="${doc.firstName} ${doc.lastName}"/></td>
				<td><c:out value="${doc.special }" /></td>
			</tr>		
		</c:forEach>
	</tbody>
</table>
</c:if>


<form method="post" action="addDoctor" >
	<select name="newDoc">
		<c:forEach items="${allDocs}" var="doc">
			<option value="${doc.id}">Dr. <c:out value="${doc.firstName} ${doc.lastName}, ${doc.special }"/></option>
		</c:forEach>
	</select>
	<input type="submit" value="Add Doc"/>
</form>

</body> 
</html>
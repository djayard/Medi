<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>

<script src="https://code.jquery.com/jquery-2.2.4.min.js" 
integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" 
crossorigin="anonymous"></script>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" 
integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="/medi/resources/js/patientApp.js"></script>

<title>Patient Profile</title>
</head>
<body ng-app="patientApp" >
	<%@ include file="header.jsp" %>
	<input id="patientId" type="hidden" value="${account.id}" readonly />
	<input id="username" type="hidden" value="${account.username}" readonly />
    <div class="container" ng-controller="MyCtrl" >
		<div class="row">
			<div class="col-md-2">
					<ul class="list-group">
						<li class="list-group-item"><a href="#profile">My Account </a></li>
						<li class="list-group-item"><a href="#manageProvider">Manage My Providers </a></li>
						<li class="list-group-item"><a href="#pendingProvider">Pending Providers </a></li>
						<li class="list-group-item"><a href="#appointment">My Appointments </a></li>
						<li class="list-group-item"><a href="#showDocuments">My Records </a></li>
						<li class="list-group-item"><a href="#testResults">My Test Results</a>
						<li class="list-group-item"><a href="#showHealth">My Health </a></li>
					</ul>
			</div>
			
			<div class="col-md-10">
				<div ng-view></div>
			</div>
			
		</div>
	</div>
</body>
</html>
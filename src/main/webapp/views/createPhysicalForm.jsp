<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>        
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<sf:form method="POST" commandName="createPysicalForm" action="" >
	<sf:errors path="*" /><br/>
	Temperature: <sf:input type="number" path="temperature" /><br/>
	Blood Pressure: <sf:input type="number" path="bloodPressure" /><br/>
	Weight: <sf:input type="number" path="weight" /><br/>
	Height: <sf:input type="number" path="height" /><br/>
	Reason: <sf:input path="reason" /><br/>
	Symptoms: <sf:input path="symptoms" /><br/>
	Allergies: <sf:input path=socialSecurity /><br/>
	Medications: <sf:input path="medications" /><br/>
	PriorIssues: <sf:input path="priorIssues"/><br/>

	
	<input type="submit" value="Register" /><br/>
</sf:form>

</body>
</html>
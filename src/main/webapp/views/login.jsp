<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="resources/css/signin.css" /> 
<link rel="stylesheet" href="resources/css/coverr.css" >
<link rel="stylesheet" href="resources/css/homepage.css" >


<script type="text/javascript" src="resources/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="resources/js/coverr.js" > </script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" 
integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<title>Login</title>
</head>
<body>
	<div class="homepage-hero-module">
	    <div id="videoContainer" class="video-container">
	        <div id="loginFormContainer" class="container">
				<sf:form class="form-signin" modelAttribute="loginForm" method="POST" action="login">
					<h1><span class="label label-info">Patient Access Portal</span></h1> 
					<sf:errors cssClass="btn btn-danger" path="*" element="span"/>
					<h2 class="form-signin-heading">Please sign in</h2>
					<label class="sr-only">Username</label>
					<sf:input path="username" type="text" id="inputEmail" class="form-control" placeholder="Username" required="" autofocus=""/>
					<label class="sr-only">Password</label>
					<sf:input path="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required="" />
					<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					<input type="submit" formaction="createUser" formmethod="get" class="btn btn-lg btn-primary btn-block" value="Register"></input>
				</sf:form>
				<div id="messageContainerWrapper" ></div>
			</div>
	        <div class="filter"></div>
	        <video autoplay loop class="fillWidth" id="cover-video">
	            <source src="resources/Coverr/All-set/MP4/All-set.mp4" type="video/mp4" />
	            <source src="resources/Coverr/All-set/WEBM/All-set.webm" type="video/webm" />
	        </video>
	        <div class="poster hidden">
	            <img src="/medi/resources/Coverr/All-set/snapshots/All-set.jpg" alt="">
	        </div>
	    </div>
	</div>
</body>
</html>
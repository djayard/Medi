<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="nurseApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome, <c:out value="${account.firstName}"/></title>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<input id="nurseId" type="hidden" value="${account.id}" readonly />
<input id="username" type="hidden" value="${account.username}" readonly />

<%@ include file="header.jsp" %>
<div class="container"  >
		<div class="row">
			<div class="col-md-2">
					<ul class="list-group">
						<li class="list-group-item"><a href="#profile">My Account </a></li>
						<li class="list-group-item"><a href="#open">View Patients</a></li>
					</ul>
			</div>
			
			<div class="col-md-10">
				<div ng-view></div>
			</div>
			
		</div>
	</div>
</body>

<script src="/medi/resources/js/nurseApp.js" ></script>
</html>
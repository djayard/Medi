<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script>
	var model = [];
	model.bodyTemp = "${model.bodyTemp}";
	model.bloodPressure = "${model.bloodPressure}";
	model.weight = "${model.weight}";
</script>
</head>
<body>
	<div id="containerPie"
		style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
	<div id="containerLine"
		style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	<div id="containerBar"
		style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
	<script>
		$(document)
				.ready(
						function() {
							var chart1 = new Highcharts.Chart(
									{
										chart : {
											plotBackgroundColor : null,
											plotBorderWidth : null,
											plotShadow : false,
											type : 'pie',
											renderTo : 'containerPie'
										},
										title : {
											text : 'Patient needs'
										},
										tooltip : {
											pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
										},
										plotOptions : {
											pie : {
												allowPointSelect : true,
												cursor : 'pointer',
												dataLabels : {
													enabled : true,
													format : '<b>{point.name}</b>: {point.percentage:.1f} %',
													style : {
														color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
																|| 'black'
													}
												}
											}
										},
										series : [ {
											name : '',
											colorByPoint : true,
											data : [ {
												name : 'Body Temp',
												y : 246
											//model.bodyTemp
											}, {
												name : 'Blood Pressure',
												y : 233, //model.bloodPressure
												sliced : true,
												selected : true
											}, {
												name : 'Weight',
												y : 123
											//model.weight
											}
											//             , {
											//                 name: 'Concentration',
											//                 y: 4.77
											//             }, {
											//                 name: 'Movement',
											//                 y: 0.91
											//             }, {
											//                 name: 'Other',
											//                 y: 0.2
											//             }
											]
										} ]
									});

							var chart2 = new Highcharts.Chart({

								chart : {
									renderTo : 'containerLine'
								},
								title : {
									text : 'Patient Results',
									x : -20
								//center
								},
								subtitle : {
									text : '',
									x : -20
								},
								xAxis : {
									categories : [ 'Jan', 'Feb', 'Mar', 'Apr',
											'May', 'Jun', 'Jul', 'Aug', 'Sep',
											'Oct', 'Nov', 'Dec' ]
								},
								yAxis : {
									title : {
										text : 'Results'
									},
									plotLines : [ {
										value : 0,
										width : 1,
										color : '#808080'
									} ]
								},
								tooltip : {
									valueSuffix : '�C'
								},
								legend : {
									layout : 'vertical',
									align : 'right',
									verticalAlign : 'middle',
									borderWidth : 0
								},
								series : [
										{
											name : 'Body Tempurature',
											data : [ 7.0, 6.9, 9.5, 14.5, 18.2,
													21.5, 25.2, 26.5, 23.3,
													18.3, 13.9, 9.6 ]
										},
										{
											name : 'Blood Pressure',
											data : [ -0.2, 0.8, 5.7, 11.3,
													17.0, 22.0, 24.8, 24.1,
													20.1, 14.1, 8.6, 2.5 ]
										},
										{
											name : 'Cholesterol',
											data : [ -0.9, 0.6, 3.5, 8.4, 13.5,
													17.0, 18.6, 17.9, 14.3,
													9.0, 3.9, 1.0 ]
										},
										{
											name : 'Weight',
											data : [ 3.9, 4.2, 5.7, 8.5, 11.9,
													15.2, 17.0, 16.6, 14.2,
													10.3, 6.6, 4.8 ]
										} ]
							});

							var chart3 = new Highcharts.Chart(
									{
										chart : {
											type : 'bar',
											renderTo : 'containerBar'
										},
										title : {
											text : 'Patient needs'
										},
										subtitle : {
											text : 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
										},
										xAxis : {
											categories : [ 'Pain Management',
													'Muscle Relaxation',
													'Improve balance',
													'Increase Strength',
													'Improve Focus' ],
											title : {
												text : null
											}
										},
										yAxis : {
											min : 0,
											title : {
												text : 'units',
												align : 'high'
											},
											labels : {
												overflow : 'justify'
											}
										},
										tooltip : {
											valueSuffix : ' millions'
										},
										plotOptions : {
											bar : {
												dataLabels : {
													enabled : true
												}
											}
										},
										legend : {
											layout : 'vertical',
											align : 'right',
											verticalAlign : 'top',
											x : -40,
											y : 80,
											floating : true,
											borderWidth : 1,
											backgroundColor : ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
											shadow : true
										},
										credits : {
											enabled : false
										},
										series : [ {
											name : 'Year 1800',
											data : [ 107, 31, 635, 203, 2 ]
										}, {
											name : 'Year 1900',
											data : [ 133, 156, 947, 408, 6 ]
										}, {
											name : 'Year 2012',
											data : [ 1052, 954, 4250, 740, 38 ]
										} ]
									});

						});
	</script>
</body>
</html>
drop table states cascade constraints;
create table STATES
(
  STATE_ID number primary key,
  STATE_NAME varchar2(50) not null,
  STATE_ABBRV varchar2(2) not null
);

drop table addresses cascade constraints;
create table ADDRESSES
(
  ADDRESS_ID number primary key,
  ADDRESS_LINE1 varchar2(30) not null,
  ADDRESS_LINE2 varchar2(15),
  CITY varchar2(20) not null,
  STATE_ID number references STATES(STATE_ID) not null,
  ZIP varchar2(10) not null,
  PHONE varchar2(12) not null,
  FAX varchar2(12)
);

drop table medical_groups cascade constraints;
create table MEDICAL_GROUPS
(
  MEDICAL_GROUP_ID number primary key,
  GROUP_NAME varchar2(50) not null,
  MAIN_ADDRESS references ADDRESSES(ADDRESS_ID)
);

drop table user_account_types cascade constraints;
create table USER_ACCOUNT_TYPES
(
  TYPE_ID number primary key,
  DESCRIPTION varchar2(30) not null
);

drop table user_accounts cascade constraints;
create table USER_ACCOUNTS
(
  ACCOUNT_ID number primary key,
  USERNAME varchar2(50),
  USER_PASSWORD varchar2(50),
  FIRST_NAME varchar2(30) not null,
  LAST_NAME varchar2(30) not null,
  E_MAIL varchar2(30),
  SPECIAL varchar2(30),
  ADDRESS number references ADDRESSES(ADDRESS_ID) not null,
  MEDICAL_GROUP_ID number references MEDICAL_GROUPS(MEDICAL_GROUP_ID) not null,
  ACCOUNT_TYPE number references USER_ACCOUNT_TYPES(TYPE_ID) not null
);

drop table registration_keys cascade constraints;
create table REGISTRATION_KEYS
(
  REGISTRATION_KEY number primary key,
  USER_ACCOUNT references USER_ACCOUNTS(ACCOUNT_ID) not null
);

drop table doctors_patients cascade constraints;
create table DOCTORS_PATIENTS
(
  DOCTOR_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  PATIENT_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  END_DATE DATE,
  primary key(DOCTOR_ID,PATIENT_ID)
);

drop table doctors_support;
create table DOCTORS_SUPPORT
(
  DOCTOR_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  SUPPORT_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  primary key (DOCTOR_ID, SUPPORT_ID)
);

drop table PATIENT_EVALUATIONS cascade constraints;
create table PATIENT_EVALUATIONS
(
  EVAL_ID number primary key,
  BODY_TEMP number,
  BLOOD_PRESSURE varchar2(7),
  WEIGHT number,
  HEIGHT number,
  REASON varchar2(140),
  SYMPTOMS varchar2(140),
  ALLERGIES varchar2(140),
  MEDICATIONS varchar2(140),
  PRIOR_ISSUES varchar2(140),
  PATIENT_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null
);

drop table RECORD_TYPE cascade constraints;
create table RECORD_TYPE
(
  TYPE_ID number primary key,
  DESCRIPTION varchar2(15) not null
);

drop table RECORDS cascade constraints;
create table RECORDS
(
  RECORD_ID number primary key,
  FILENAME varchar2(100) not null,
  TYPE_ID number references RECORD_TYPE(TYPE_ID) not null,
  TARGET_ID number not null
);

drop table TEST_TYPES cascade constraints;
create table TEST_TYPES
(
  TYPE_ID number primary key,
  TEST_CODE varchar(12) not null
);

drop table tests cascade constraints;
create table TESTS
(
  TEST_ID number primary key,
  DOCTOR_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  PATIENT_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  DATE_ORDERED Date not null
);

drop table TEST_LINES cascade constraints;
create table TEST_LINES
(
  TEST_ID number references TESTS(TEST_ID) not null,
  LINE_ID number not null,
  TEST_CODE number references TEST_TYPES(TYPE_ID) not null,
  TEST_RESULT varchar2(255),
  DATE_COMPLETED Date,
  primary key(TEST_ID,LINE_ID)
);

drop table PRESCRIPTIONS cascade constraints;
create table PRESCRIPTIONS
(
  PRESCRIPTION_ID number primary key,
  DOCTOR_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  PATIENT_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  DATE_ORDERED date
);

drop table medications cascade constraints;
create table MEDICATIONS
(
  MEDICATION_ID number primary key,
  MEDICATION_NAME varchar2(25) not null,
  CHEMICAL_NAME varchar2(25),
  PRODUCER varchar2(50)
);

drop table prescription_lines cascade constraints;
create table PRESCRIPTION_LINES
(
  PRESCRIPTION_ID references PRESCRIPTIONS(PRESCRIPTION_ID),
  LINE_ID number,
  QUANTITY number not null,
  UNITS varchar2(10) not null,
  REFILLS number not null,
  MEDICATION number references MEDICATIONS(MEDICATION_ID) not null,
  primary key(PRESCRIPTION_ID,LINE_ID)
);

drop table payment_types cascade constraints;
create table PAYMENT_TYPES
(
  TYPE_ID number primary key,
  DESCRIPTION varchar2(10) not null
);

drop table payments cascade constraints;
create table PAYMENTS
(
  PAYMENT_ID number primary key,
  PAYMENT_TYPE number references PAYMENT_TYPES(TYPE_ID) not null,
  BILLED_NAME varchar2(100) not null,
  BIlLED_ADDRESS number references ADDRESSES(ADDRESS_ID) not null
);

drop table appointments cascade constraints;
create table APPOINTMENTS
(
  APPOINTMENT_ID number primary key,
  APPOINTMENT_TIME DATE not null,
  PATIENT_ID number references USER_ACCOUNTS(ACCOUNT_ID),
  DOCTOR_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  PATIENT_EVAL number references PATIENT_EVALUATIONS(EVAL_ID),
  ORDERED_TESTS references TESTS(TEST_ID),
  ORDERED_MEDS references PRESCRIPTIONS(PRESCRIPTION_ID),
  PAYMENT_ID number references PAYMENTS(PAYMENT_ID)
);

drop table immunization_types cascade constraints;
create table IMMUNIZATION_TYPES
(
  TYPE_ID number primary key,
  TREATMENT_NAME varchar2(30) not null
);

drop table immunization_records cascade constraints;
create table IMMUNIZATION_RECORDS
(
  RECORD_ID number primary key,
  PATIENT_ID number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  TREATER number references USER_ACCOUNTS(ACCOUNT_ID) not null,
  TREATMENT_DATE Date not null,
  TREATMENT_TYPE number references IMMUNIZATION_TYPES(TYPE_ID) not null
);




drop sequence ADDRESS_SEQ;
drop sequence APPOINTMENT_SEQ;
drop sequence DOCTOR_SEQ;
drop sequence EXAM_SEQ;
drop sequence IMMU_RECORD_SEQ;
drop sequence IMMU_TYPE_SEQ;
drop sequence MED_GROUP_SEQ;
drop sequence MEDICATION_SEQ;
drop sequence NURSE_SEQ;
drop sequence PATIENT_TYPE_SEQ;
drop sequence PHYS_STAT_SEQ;
drop sequence PRESCRIPTION_SEQ;
drop sequence STATE_SEQ;
drop sequence TEST_TYPE_SEQ;
drop sequence TEST_SEQ;
drop sequence USER_ACCT_TYPE_SEQ;
drop sequence USER_ACCT_SEQ;
drop sequence PAYMENT_SEQ;
drop sequence PAYMENT_TYPE_SEQ;

create sequence ADDRESS_SEQ start with 1 increment by 1;
create sequence APPOINTMENT_SEQ start with 1 increment by 1;
create sequence DOCTOR_SEQ start with 1 increment by 1;
create sequence EXAM_SEQ start with 1 increment by 1;
create sequence IMMU_RECORD_SEQ start with 1 increment by 1;
create sequence IMMU_TYPE_SEQ start with 1 increment by 1;
create sequence MED_GROUP_SEQ start with 1 increment by 1;
create sequence MEDICATION_SEQ start with 1 increment by 1;
create sequence NURSE_SEQ start with 1 increment by 1;
create sequence PATIENT_TYPE_SEQ start with 1 increment by 1;
create sequence PHYS_STAT_SEQ start with 1 increment by 1;
create sequence PRESCRIPTION_SEQ start with 1 increment by 1;
create sequence STATE_SEQ start with 1 increment by 1;
create sequence TEST_TYPE_SEQ start with 1 increment by 1;
create sequence TEST_SEQ start with 1 increment by 1;
create sequence USER_ACCT_TYPE_SEQ start with 1 increment by 1;
create sequence USER_ACCT_SEQ start with 1 increment by 1;
create sequence PAYMENT_SEQ start with 1 increment by 1;
create sequence PAYMENT_TYPE_SEQ start with 1 increment by 1;
create sequence RECORD_SEQ start with 1 increment by 1;



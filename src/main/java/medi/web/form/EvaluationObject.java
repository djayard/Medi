package medi.web.form;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EvaluationObject {
	private Integer appointmentId;
	private String patientName;
	private String time;
	private String doctorName;
	private PatientEvaluationForm patientEvaluation; //non persisted
	
	public EvaluationObject(){
	}

	public EvaluationObject(Date time, PatientEvaluationForm patientEvaluation){
		SimpleDateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy");
		this.time = simpleFormat.format(time);
		this.patientEvaluation = patientEvaluation;
	}
	
	public EvaluationObject(Integer appointmentId, String patientName, Date time, String doctorName, PatientEvaluationForm patientEvaluation) {
		super();
		this.appointmentId = appointmentId;
		this.patientName = patientName;
		SimpleDateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy");
		this.time = simpleFormat.format(time);
		this.doctorName = doctorName;
		this.patientEvaluation = patientEvaluation;
	}

	
	public Integer getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(Integer appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(Date time) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy");
		this.time = simpleFormat.format(time);
	}

	public PatientEvaluationForm getPatientEvaluation() {
		return patientEvaluation;
	}

	public void setPatientEvaluation(PatientEvaluationForm patientEvaluation) {
		this.patientEvaluation = patientEvaluation;
	}

	@Override
	public String toString() {
		return "EvaluationObject [appointmentId=" + appointmentId + ", patientName=" + patientName + ", time=" + time
				+ ", doctorName=" + doctorName + ", patientEvaluation=" + patientEvaluation + "]";
	}
	
}

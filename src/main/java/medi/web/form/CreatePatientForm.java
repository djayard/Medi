package medi.web.form;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;


//using ScopedProxyMode set to TARGET_CLASS since we are not implementing any interface here
@Component
@Scope(value=WebApplicationContext.SCOPE_REQUEST,
		proxyMode=ScopedProxyMode.TARGET_CLASS)
public class CreatePatientForm {
	
	@NotEmpty(message="Username must not be empty.")
	private String username;
	@NotEmpty(message="Password must not be empty.")
	private String password;
	@NotEmpty(message="First name must not be empty.")
	private String firstName;
	@NotEmpty(message="Last name must not be empty.")
	private String lastName;
	@NotEmpty(message="E-mail must not be empty.")
	@Email(message="not a well formed email")
	private String email;
	@NotEmpty(message="Social Security must not be empty.")
	@Pattern(regexp="^(\\d{3}-\\d{2}-\\d{4})$",message="Social Security must by 9 digits long.")
	private String socialSecurity;
	//@NotEmpty(message="insurance Id must not be empty")
	//private String insuranceID;
	@NotEmpty(message="Address must not be empty.")
	private String address1;
	private String address2;
	@NotEmpty(message="City must not be empty.")
	private String city;

	@NotEmpty(message="State must not be empty.")
	private String state;
	@NotEmpty(message="ZIP code must not be empty.")
	@Pattern(regexp="^\\d{5}(-\\d{4})?$", message="ZIP must be at least 5 digits long.")
	private String zip;
	@NotEmpty(message="A primary phone number is required.")
	@Pattern(regexp="^(\\d{3}-\\d{3}-\\d{4})$", message="Invalid Phone number format; please use ###-###-####")
	private String phone;
	@NotEmpty(message="Date of birth must not be empty.")
	@Pattern(regexp="\\d{2}-\\d{2}-\\d{2}", message="Invalid date format; please use dd-mm-yy")
	private String dateOfBirth;
	private String sex;
	
	public CreatePatientForm(){
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getSocialSecurity() {
		return socialSecurity;
	}

	public void setSocialSecurity(String socialSecurity) {
		this.socialSecurity = socialSecurity;
	}


	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
}

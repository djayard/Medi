package medi.web.form;

public class PatientEvaluationForm {

	private Double bodyTemp;
	private String bloodPressure;
	private Integer heartRate;
	private Double bloodSugar;
	private Double weight;
	private String height;
	private String reason;
	private String symptoms;
	private String allergies;
	private String medications;
	private String priorIssues;
	private Integer patientId;
	
	
	public Integer getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(Integer heartRate) {
		this.heartRate = heartRate;
	}

	public Double getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(Double bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	
	
	public Double getBodyTemp() {
		return bodyTemp;
	}
	public void setBodyTemp(Double bodyTemp) {
		this.bodyTemp = bodyTemp;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSymptoms() {
		return symptoms;
	}
	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}
	public String getAllergies() {
		return allergies;
	}
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	public String getMedications() {
		return medications;
	}
	public void setMedications(String medications) {
		this.medications = medications;
	}
	public String getPriorIssues() {
		return priorIssues;
	}
	public void setPriorIssues(String priorIssues) {
		this.priorIssues = priorIssues;
	}
	public Integer getPatientId() {
		return patientId;
	}
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	@Override
	public String toString() {
		return "PatientEvaluation [bodyTemp=" + bodyTemp + ", bloodPressure=" + bloodPressure + ", weight=" + weight
				+ ", height=" + height + ", reason=" + reason + ", symptoms=" + symptoms + ", allergies=" + allergies
				+ ", medications=" + medications + ", priorIssues=" + priorIssues + ", patientId=" + patientId + "]";
	}
	
	
}

package medi.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import dao.MediDAO;
import dao.MediInterface;

import medi.web.services.*;

@EnableWebMvc
@Configuration
@ComponentScan({ "medi.web.controller", "medi.web.form", "medi.web.services" })
public class SpringWebConfig extends WebMvcConfigurerAdapter {

	/**
	 * Add handlers to serve static resources such as images, js,
	 *  and, css files from specific locations under web application 
	 *  root, the classpath, and others.
	 * 
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	
	@Bean
	public BCryptPasswordEncoder getBCryptPasswordEncoder(){
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource rb = new ResourceBundleMessageSource();
		rb.setBasenames(new String[] { "messages/messages", "messages/validation" });
		return rb;
	}
	
	@Bean
	public MultipartResolver multipartResolver()
	{
		return new CommonsMultipartResolver();
	}
	
	@Bean
	public FileService fileService(){
		return new FileServiceImpl();
	}

}
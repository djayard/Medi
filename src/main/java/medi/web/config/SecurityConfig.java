package medi.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import dao.MediInterface;
import medi.web.security.AccountSecurity;

/**
 * 
 * @author yans_pc
 *
 * SecurityConfig is a configuration source class, 
 * @EnableWebSecurity annotation will create a few security filters for us automatically 
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	MediInterface mediInterface; 
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	//Override to configure how the requests are secured by the interceptor
	@Override
	protected void configure( HttpSecurity http ) throws Exception{
		//http.authorizeRequests().anyRequest().permitAll();
		
		http.authorizeRequests()
		.antMatchers("/home", "/login", "/resources/**").permitAll()
 		.antMatchers("/patient/**").hasRole("PATIENT")
 		.antMatchers("/assistant/**").hasRole("NURSE")
 		.antMatchers("/provider/**").hasRole("DOCTOR")
 		.anyRequest().permitAll()
 		.and()
 		.formLogin().loginPage("/login").successForwardUrl("/loginSuccess").failureForwardUrl("/loginError")
 		.and().logout().logoutSuccessUrl("/login")
 		.and()
 		.httpBasic()
 		.and().csrf().disable();
 		
	}
	
	//Override to configure to configure user detail services
	@Override 
	public void configure( AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(new AccountSecurity(mediInterface) ).passwordEncoder(bCryptPasswordEncoder);
	}
	
	
}

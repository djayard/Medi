package medi.web.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import dao.MediInterface;
import entities.Account;
import entities.PatientEvaluation;
import entities.Record;
import entities.Test;
import entities.TestLine;
import medi.web.form.EditProfile;
import medi.web.form.EvaluationObject;
import medi.web.services.AccountService;
import medi.web.services.DocumentObject;
import medi.web.services.FileService;

@Controller
@RequestMapping(value="patient")
public class PatientController {
	
	private MediInterface mediInterface;
	private AccountService accountService;
	private FileService fileService;
	
	public PatientController(){
	}
			
	@Autowired
	public void setMediInterface( MediInterface mediInterface ){
		this.mediInterface = mediInterface;
	}
	
	@Autowired
	public void setAccountService( AccountService accountService ){
		this.accountService = accountService;
	}
	
	@Autowired
	public void setFileService(FileService fileService){
		this.fileService = fileService;
	}
	
	@RequestMapping(value="profile", method=RequestMethod.GET)
	public String showProfile(HttpServletRequest request, Model model) throws Exception{
		HttpSession session = request.getSession(false);
		if(session == null){
			throw new Exception("Session Does Not Exist");
		}
		Account account = (Account) session.getAttribute("account");
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		session.setAttribute("account", account);
		return "patientProfile";
	}
	
	@RequestMapping(value="getProfile", method=RequestMethod.GET )
	public @ResponseBody Account getAccount( @RequestParam("username") String username ){
		Account account =  accountService.getAccountByName(username);
		
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		
		Account ret = new Account();
		ret.setId(account.getId());
		ret.setUsername(account.getUsername());
		ret.setFirstName(account.getFirstName());
		ret.setLastName(account.getLastName());
		ret.seteMail(account.geteMail());
		ret.setAddress(account.getAddress());
		ret.setSex(account.getSex());
		ret.setDob(account.getDob());
		return ret;
	}
	
	@RequestMapping(value="editProfile", method=RequestMethod.POST, consumes="application/json")
	public @ResponseBody Account editAccount( @RequestBody EditProfile editProfile ){
		Account account = accountService.editProfile(editProfile);
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		
		Account ret = new Account();
		ret.setId(account.getId());
		ret.setUsername(account.getUsername());
		ret.setFirstName(account.getFirstName());
		ret.setLastName(account.getLastName());
		ret.seteMail(account.geteMail());
		ret.setAddress(account.getAddress());
		return ret;
	}
	
	@RequestMapping(value="subscribe", method=RequestMethod.POST)
	public @ResponseBody Boolean makeRequest( @RequestBody Map<String,Integer> requestParams ){
		Integer patientId = requestParams.get("patientId");
		Integer doctorId = requestParams.get("providerId");
		return mediInterface.createNewRequest(patientId, doctorId);
	}
	
	
	//pendingProviderController
	@RequestMapping(value="getPendingProviders", method=RequestMethod.GET )
	public @ResponseBody List<Account> getPendingProvider(@RequestParam("patientId") Integer patientId){
		return accountService.getPendingProvidersByPatientId(patientId); 
	}
	
	/**
	 * @return JSON list of ACCOUNTs
	 * getProviders returns a list of primary physicians available to the id
	 * the client accept-header should include application/json
	 * the client content-type should include text/plain 
	 * 
	 */
	@RequestMapping(value="findAvailableProviders", method=RequestMethod.GET)
	public @ResponseBody List<Account> getAvailableProviders( @RequestParam("patientId") int userID ){
		List<Account> accounts =  accountService.getAvailableProvidersByUserID(userID);
		return accounts;
	}
	
	@RequestMapping(value="findCurrentProviders", method=RequestMethod.GET)
	public @ResponseBody List<Account> getCurrentProviders( @RequestParam("patientId") int userID ){
		List<Account> accounts =  accountService.getCurrentProvidersByUserID(userID);
		return accounts;
	}
	
	
	/**
	 * @param map 
	 * @return logical view name
	 */
	@RequestMapping(value="unsubscribe", method=RequestMethod.POST )
	public @ResponseBody Boolean removeDoctor( @RequestBody Map<String,Integer> requestParams ){
		Integer patientId = requestParams.get("patientId");
		Integer doctorId = requestParams.get("providerId");
		
		System.out.println("patient id:"+patientId);
		System.out.println("doctor id:"+doctorId);
		return mediInterface.removeDoctor(patientId, doctorId);
		
	}
	
	@RequestMapping(value="showDocuments", method=RequestMethod.GET )
	public @ResponseBody List<?> getDocument( @RequestParam("username") String username, 
			@RequestParam("docType") String type ) 
			throws Exception{ //we need to pass REST an exception
		return accountService.getDocuments(username, type);
	}
	
	//we need to turn these into REST requests
	@RequestMapping(value="doctorPermitControl", method=RequestMethod.GET)
	public String addDocotor(HttpServletRequest map){
		map.setAttribute("approvedDocs", mediInterface.getPatientsDoctors(-3));
		map.setAttribute("allDocs", mediInterface.getUnusedDoctors(-3));
		return "addDoctor";
	}
	
	@RequestMapping(value="addDoctor", method=RequestMethod.POST)
	public String updatePermission(HttpServletRequest req){
		Account patient = (Account) req.getSession().getAttribute("patient");
		mediInterface.addDoctor( -3, Integer.parseInt(req.getParameter("newDoc")));
		return "success";
	}
		
	@RequestMapping(value="getPatientRecords", method=RequestMethod.GET)
	public @ResponseBody List<DocumentObject> getPatientRecord(@RequestParam("patientId") Integer patientId ){
		List<Record> records = accountService.getPatientRecords(patientId);
		List<String> publicList = fileService.getFileLinks( records );
		List<DocumentObject> docObjects = accountService.generateDocumentObjects(records, publicList);
		return docObjects;
	}
	
	@RequestMapping(value="getPatientEvaluations", method=RequestMethod.GET)
	public @ResponseBody List<EvaluationObject> getPatientEvaluations(@RequestParam("patientId") Integer patientId){
		System.out.println("[PatientController][getPatientEvaluations]"); 
		List<EvaluationObject> result = accountService.getPatientEvaluations(patientId);
		Collections.reverse(result);
		return result;
	}
	
	@RequestMapping(value="getPatientBloodPressure", method=RequestMethod.GET)
	public @ResponseBody Map<Integer, ArrayList<? extends Number>> getPatientBloodPressure(@RequestParam("patientId") Integer patientId ){
		return accountService.getBloodPressure(patientId);
	}
	
	@RequestMapping(value="getPatientWeight", method=RequestMethod.GET)
	public @ResponseBody Map<Integer, ArrayList<? extends Number>> getPatientWeight(@RequestParam("patientId") Integer patientId)
	{
		return accountService.getWeight(patientId);
	}
	

	@RequestMapping(value="searchProviderMatch", method=RequestMethod.GET )
	public @ResponseBody List<Account> searchProviderMatch(@RequestParam("providerMatch") String providerMatch,
			@RequestParam("patientId") Integer patientId){
		List<Account> accounts = accountService.searchProviderMatch(providerMatch, patientId);
		return accounts;
	}
	
	@RequestMapping(value="getPatientBloodSugar", method=RequestMethod.GET)
	public @ResponseBody Map<Integer, ArrayList<? extends Number>> getPatientBloodSugar(@RequestParam("patientId") Integer patientId)
	{
		return accountService.getBloodSugar(patientId);
	}
	
	@RequestMapping(value="getPatientHeartRate", method=RequestMethod.GET)
	public @ResponseBody Map<Integer, ArrayList<? extends Number>> getPatientHeartRate(@RequestParam("patientId") Integer patientId)
	{
		return accountService.getHeartRate(patientId);
	}
	
	@RequestMapping(value="getTestResults", method =  RequestMethod.GET )
	public @ResponseBody Map<String,Object>[] getTestResults(@RequestParam("id") Integer patientId)
	{
		Test test = mediInterface.getTests(patientId);
		List<TestLine> lines = test.getLines();
		Collections.sort(lines);
		
		Map<String,Object>[] result = new Map[lines.size()];
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yy");
		for(int i = 0; i < lines.size(); ++i){
			result[i] = new HashMap<String,Object>();
			result[i].put("date", fmt.format(lines.get(i).getCompletionDate()));
			result[i].put("type", lines.get(i).getType());
			result[i].put("result", lines.get(i).getResult());
		}
		
		return result;
	}
	
}

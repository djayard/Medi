package medi.web.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import entities.Account;
import entities.Record;
import medi.web.form.EditProfile;
import medi.web.form.EvaluationObject;
import medi.web.services.AccountService;
import medi.web.services.DocumentObject;
import medi.web.services.FileService;

@Controller
@RequestMapping(value="provider")
public class ProviderController {

	private FileService fileService;
	private AccountService accountService;
	
	public ProviderController(){
	}
	
	@Autowired
	public void setAccountService( AccountService accountService ){
		this.accountService = accountService;
	}
	
	@Autowired
	public void setFileService(FileService fileService){
		this.fileService = fileService;
	}
	
	@RequestMapping(value="profile", method=RequestMethod.GET)
	public String showProfile(HttpServletRequest request, Model model) throws Exception{
		HttpSession session = request.getSession(false);
		if(session == null){
			throw new Exception("Session Does Not Exist");
		}
		Account account = (Account) session.getAttribute("account");
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		session.setAttribute("account", account);
		
		List<Account> pendingPatients = accountService.getPendingPatientByProviderId(account.getId());
		session.setAttribute("pendingPatients", pendingPatients.size());
		
		return "doctorProfile";
	}
	
	@RequestMapping(value="getNumberPending", method=RequestMethod.GET)
	public @ResponseBody Integer getNumberPending(@RequestParam("providerId") Integer providerId ){
		List<Account> pendingPatients = accountService.getPendingPatientByProviderId(providerId);
		return pendingPatients.size();
	}
	
	@RequestMapping(value="getPending", method=RequestMethod.GET)
	public @ResponseBody List<Account> getPendingRequest(@RequestParam("providerId") Integer providerId ){
		return accountService.getPendingPatientByProviderId(providerId);
	}
	
	
	@RequestMapping(value="getPatients", method=RequestMethod.GET)
	public @ResponseBody List<Account> getProviderPatients(@RequestParam("providerId") Integer providerId){
		return accountService.getActivePatientsByProviderID(providerId);
	}
	
	@RequestMapping(value="approvePatient", method=RequestMethod.POST)
	public @ResponseBody Boolean approveUser(@RequestBody Map<String, Integer> requestParam){
		Integer patientId = requestParam.get("patientId");
		Integer providerId = requestParam.get("providerId");
		return accountService.approvePatient(patientId, providerId);
	}
	
	@RequestMapping(value="denyPatient", method=RequestMethod.POST)
	public @ResponseBody Boolean denyUser(@RequestBody Map<String, Integer> requestParam){
		Integer patientId = requestParam.get("patientId");
		Integer providerId = requestParam.get("providerId");
		return accountService.denyPatient(patientId, providerId);
	}
	
	
	@RequestMapping(value="getProfile", method=RequestMethod.GET )
	public @ResponseBody Account getAccount( @RequestParam("username") String username ){
		Account account =  accountService.getAccountByName(username);
		
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		
		Account ret = new Account();
		ret.setId(account.getId());
		ret.setUsername(account.getUsername());
		ret.setFirstName(account.getFirstName());
		ret.setLastName(account.getLastName());
		ret.seteMail(account.geteMail());
		ret.setAddress(account.getAddress());
		return ret;
	}
	
	@RequestMapping(value="editProfile", method=RequestMethod.POST, consumes="application/json")
	public @ResponseBody Account editAccount( @RequestBody EditProfile editProfile ){
		Account account = accountService.editProfile(editProfile);
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		
		Account ret = new Account();
		ret.setId(account.getId());
		ret.setUsername(account.getUsername());
		ret.setFirstName(account.getFirstName());
		ret.setLastName(account.getLastName());
		ret.seteMail(account.geteMail());
		ret.setAddress(account.getAddress());
		return ret;
	}
	
	@RequestMapping(value="getPatientRecords", method=RequestMethod.GET)
	public @ResponseBody List<DocumentObject> getPatientRecord(@RequestParam("patientId") Integer patientId ){
		List<Record> records = accountService.getPatientRecords(patientId);
		List<String> publicList = fileService.getFileLinks( records );
		List<DocumentObject> docObjects = accountService.generateDocumentObjects(records, publicList);
		return docObjects;
	}
	
	@RequestMapping(value="getPatientEvaluations", method=RequestMethod.GET)
	public @ResponseBody List<EvaluationObject> getPatientEvaluations(@RequestParam("patientId") Integer patientId){
		List<EvaluationObject> result = accountService.getPatientEvaluations(patientId);
		Collections.reverse(result);
		return result;
	}

	@RequestMapping(value="uploadFile", method=RequestMethod.POST)
	public @ResponseBody String uploadFile(@RequestParam("file") MultipartFile file, 
			@RequestParam("patientId") Integer id, @RequestParam("type") String type){
		String key = fileService.upload(id, file);
		if( key.isEmpty() )
			return "Failed";
		
		accountService.updateRecords(id, key, type);
		return "Success";
	}
	
	@RequestMapping(value="searchPatientMatch", method=RequestMethod.GET )
	public @ResponseBody List<Account> searchPatientMatch(@RequestParam("patientMatch") String patientMatch,
			@RequestParam("providerId") Integer providerId){
		System.out.println("patientMatch:"+patientMatch);
		List<Account> accounts =  accountService.searchPatientMatch(patientMatch, providerId);
		System.out.println("search result size = "+accounts.size());
		return accounts;
	}
}

package medi.web.controller;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import dao.MediInterface;
import entities.Account;
import entities.PatientEvaluation;
import entities.Test;
import entities.TestLine;
import medi.web.form.PatientEvaluationForm;
import medi.web.form.EditProfile;
import medi.web.services.AccountService;

@Controller
@RequestMapping(value="assistant")
public class NurseController {

	private MediInterface mediInterface;
	private AccountService accountService;
	
	@Autowired
	public void setMediInterface( MediInterface mediInterface ){
		this.mediInterface = mediInterface;
	}
	
	@Autowired
	public void setAccountService( AccountService accountService ){
		this.accountService = accountService;
	}
	
	@RequestMapping(value="profile", method = RequestMethod.GET)
	public String showProfile(HttpServletRequest request, Model model){
		HttpSession session = request.getSession(false);
		if(session == null){
			return "/login";
		}
		Account account = (Account) session.getAttribute("account");
		
		System.out.println("account name retrived:"+account.getUsername());
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social secuirty
		model.addAttribute("account", account);
		return "nurseProfile";
	}
	
	@RequestMapping(value="patientEvaluation", method = RequestMethod.POST)
	public String createPhysical(  PatientEvaluationForm physicalform, Errors errors ){
		if(errors.hasErrors())
			return "createPhysicalForm";

		return "profile";
	}
	
	@RequestMapping(value="getPatients", method= RequestMethod.GET)
	@ResponseBody
	public List<Account> getPatients(@RequestParam Integer id)
	{
		return accountService.getNursePatients(id);
	}
	
	@RequestMapping(value="submitAppointment", method=RequestMethod.POST, produces={"text/plain"})
	@ResponseBody
	public String submitAppointment(@RequestBody PatientEvaluationForm model)
	{
		System.out.println(model);
		
		PatientEvaluation eval = new entities.PatientEvaluation();
		eval.setAllergies(model.getAllergies());
		eval.setBloodPressure(model.getBloodPressure());
		eval.setBloodSugar(model.getBloodSugar());
		eval.setBodyTemp(model.getBodyTemp());
		eval.setHeartRate(model.getHeartRate());
		eval.setHeight(model.getHeight());
		eval.setMedications(model.getMedications());
		eval.setPriorIssues(model.getPriorIssues());
		eval.setReason(model.getReason());
		eval.setSymptoms(model.getSymptoms());
		eval.setWeight(model.getWeight());
		
		System.out.println(model);
		
		if( mediInterface.createAppointment(eval, model.getPatientId()))
			return "success";
		else
			return "failure";
	}
	
	@RequestMapping(value="tests", method=RequestMethod.GET)
	@ResponseBody
	public Map<String,Object>[] getTests(@RequestParam("id") Integer patientId)
	{
		Test test = mediInterface.getTests(patientId);
		List<TestLine> lines = test.getLines();
		Collections.sort(lines);
		
		Map<String,Object>[] result = new Map[lines.size()];
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yy");
		for(int i = 0; i < lines.size(); ++i){
			result[i] = new HashMap<String,Object>();
			result[i].put("date", fmt.format(lines.get(i).getCompletionDate()));
			result[i].put("type", lines.get(i).getType());
			result[i].put("result", lines.get(i).getResult());
		}
		
		return result;
	}
	
	@RequestMapping(value="updateTest", method=RequestMethod.POST, consumes={"application/json"})
	@ResponseBody
	public String updateTest(@RequestBody HashMap<String,String> param){
		TestLine line = new TestLine();
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yy");
		
		line.setType(param.get("type"));
		line.setResult(param.get("result"));
		try{
			line.setCompletionDate( fmt.parse(param.get("date")));
		}catch(Exception e){
			line.setCompletionDate(new Date());
		}
		Integer patientId = Integer.parseInt(param.get("patientId"));
		
		
		mediInterface.updateTest(patientId, line);
		
		return "success";
	}
	
	@RequestMapping(value="getProfile", method=RequestMethod.GET )
	public @ResponseBody Account getAccount( @RequestParam("username") String username ){
		Account account =  accountService.getAccountByName(username);
		
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		
		Account ret = new Account();
		ret.setId(account.getId());
		ret.setUsername(account.getUsername());
		ret.setFirstName(account.getFirstName());
		ret.setLastName(account.getLastName());
		ret.seteMail(account.geteMail());
		ret.setAddress(account.getAddress());
		return ret;
	}
	
	@RequestMapping(value="editProfile", method=RequestMethod.POST, consumes="application/json")
	public @ResponseBody Account editAccount( @RequestBody EditProfile editProfile ){
		Account account = accountService.editProfile(editProfile);
		account.setPassword(""); //empty the password
		account.setSocialSecurity(""); //zero the social security
		
		Account ret = new Account();
		ret.setId(account.getId());
		ret.setUsername(account.getUsername());
		ret.setFirstName(account.getFirstName());
		ret.setLastName(account.getLastName());
		ret.seteMail(account.geteMail());
		ret.setAddress(account.getAddress());
		return ret;
	}
	
}
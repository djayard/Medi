package medi.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import dao.MediInterface;
import entities.Account;
import entities.State;
import entities.TestLine;
import medi.web.form.CreatePatientForm;
import medi.web.form.LoginForm;
import medi.web.services.AccountService;

@Controller
public class HomeController 
{

	private MediInterface mediInterface;
	private AccountService accountService;
	
	@Autowired
	public HomeController( MediInterface mediInterface, AccountService accountService ){
		this.mediInterface = mediInterface;
		this.accountService = accountService;
	}
	
	@RequestMapping(value="getStateAbbrv", method = RequestMethod.GET)
	public @ResponseBody List<State> getStates(){
		return accountService.getStates();
	}
	
	@RequestMapping(value="/home", method = RequestMethod.GET)
	public String home(){
		return "home";
	}
	
	@RequestMapping(value={"/login","index.html"}, method=RequestMethod.GET)
	public String getLoginForm(HttpServletRequest request, Model model){
		String referrer = request.getHeader("Referrer");
		request.getSession().setAttribute("url_prior_login", referrer);
		model.addAttribute("loginForm", new LoginForm());
		return "login";
	}
	
	@RequestMapping(value="/loginError", method=RequestMethod.POST)
	public ModelAndView loginError( HttpServletRequest req, @ModelAttribute("loginForm") @Valid LoginForm loginForm , 
			Errors errors ){
		if(loginForm.getUsername() != null && loginForm.getPassword() != null )
			if( !loginForm.getUsername().isEmpty() && !loginForm.getPassword().isEmpty())
				errors.reject(null, "Invalid username and password combination.");
		
		return new ModelAndView("login");
		
	}
	
	@RequestMapping(value="/loginSuccess", method=RequestMethod.POST)
	public ModelAndView loginSuccess( HttpServletRequest req, @ModelAttribute("loginForm") @Valid LoginForm loginForm , 
			Errors errors ) throws Exception{
		
		if(errors.hasErrors())
			return new ModelAndView("login");
		
		String username = loginForm.getUsername();
		Account account = accountService.getAccountByName(username);
		String rout = accountService.routToProperPath(account);
		
		HttpSession session = req.getSession(false);
		
		if( session == null ){
			throw new Exception("Session Does Not Exist");
		}else{
			session.setAttribute("account", account);
		}
		
		System.out.println("successful login rout:"+rout);
		return new ModelAndView("redirect:/"+rout);
	}
	

	@RequestMapping(value="/signout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    return "redirect:/login?signout";
	}
	
	//we will need a controller autowiring
	@RequestMapping(value="createUser",  method = RequestMethod.GET)
	public String showClientForm(HttpServletRequest request, Model model){
		//we need to add this form object into the model for the model to work
		model.addAttribute("createUser", new CreatePatientForm()); 
		request.getSession().setAttribute("states", mediInterface.getList(State.class));
		return "createPatientForm";
	}
	
	@RequestMapping(value="createUser", method = RequestMethod.POST)
	public ModelAndView createClient(HttpServletRequest request, @ModelAttribute("createUser") @Valid CreatePatientForm createUser,
			Errors errors ) throws Exception
	{
		if(errors.hasErrors()){
			System.out.println("create user error: "+errors.toString());
			return new ModelAndView("createPatientForm");
		}
		Account account = accountService.createPatientAccount(createUser);
		
		HttpSession session = request.getSession(false);
		if(session == null){
			throw new Exception("Session Does Not Exist");
		}
		session.setAttribute("account", account);
		return new ModelAndView("redirect:/patient/profile");
	}
	
}
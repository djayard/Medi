package medi.web.services;

public class DocumentObject {
	
	private String documentType;
	private String documentLocation;
	private String documentName;
	
	public DocumentObject(){
	}
	
	public DocumentObject(String documentType, String documentLocation, String documentName){
		this.documentType = documentType;
		this.documentLocation = documentLocation;
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentLocation() {
		return documentLocation;
	}

	public void setDocumentLocation(String documentLocation) {
		this.documentLocation = documentLocation;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
}

package medi.web.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import dao.MediInterface;
import entities.Account;
import entities.AccountType;
import entities.Address;
import entities.Appointment;
import entities.PatientEvaluation;
import entities.Record;
import entities.State;
import medi.web.form.CreatePatientForm;
import medi.web.form.EditProfile;
import medi.web.form.EvaluationObject;
import medi.web.form.PatientEvaluationForm;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//exposing the AccountSetvice interface through the ScopedProxyMode
@Service
@Scope(value=WebApplicationContext.SCOPE_REQUEST,
		proxyMode=ScopedProxyMode.INTERFACES)
public class AccountServiceImpl implements AccountService {
	
	private MediInterface mediInterface;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	public AccountServiceImpl(MediInterface mediInterface ){
		this.mediInterface = mediInterface;
	}
	
	public Account getAccountByName(String username) {
		return mediInterface.getAccountByName(username);
	}
	
	
	public List<?> getDocuments( String username, String type ) throws Exception{
		Account account = getAccountByName(type);
		if(type.equals("test"))
			return account.getTests();
		else if( type.equals("appointment") )
			return account.getAppointments();
		else if( type.equals("immune") )
			return account.getImmunRecord();
		else
			throw new Exception("Invalid Document Type");
	}
	
	/** 
	 * get the list of available providers 
	 */
	public List<Account> getAvailableProvidersByUserID( Integer userId) {
		List<Account> accounts = mediInterface.getUnusedDoctors(userId);
		return getNonePersistentAccounts(accounts);
	}
	
	
	public List<Account> getCurrentProvidersByUserID(Integer userID) {
		List<Account> accounts = mediInterface.getPatientsDoctors(userID);
		return getNonePersistentAccounts(accounts);
	}
	
	
	public List<Account> getPendingProvidersByPatientId(Integer patientId) {
		List<Account> accounts = mediInterface.patientGetPendingRequests(patientId);
		return getNonePersistentAccounts(accounts);
	}
	
	
	public List<Account> getPendingPatientByProviderId(Integer providerId) {
		List<Account> accounts = mediInterface.getPendingRequests(providerId);
		return getNonePersistentAccounts(accounts);
	}
	
	
	public List<Account> getActivePatientsByProviderID(Integer providerId) {
		List<Account> accounts = mediInterface.getDoctorsPatients(providerId);
		return getNonePersistentAccounts(accounts);
	}
	
	public List<Account> getNursePatients(Integer nurseId)
	{
		List<Account> accounts = mediInterface.getNursesPatients(nurseId);
		return getNonePersistentAccounts(accounts);
	}
	
	public List<Account> searchProviderMatch(String providerMatch, Integer patientId ){
		List<Account> accounts = mediInterface.findDoctor(providerMatch, patientId );
		return getNonePersistentAccounts(accounts);
	}
	
	public List<Account> searchPatientMatch(String patientMatch, Integer providerId){
		List<Account> accounts = mediInterface.findPatient(patientMatch, providerId);
		return getNonePersistentAccounts(accounts);
	}
	
	//not working at the moment
	public void subScribeToDoctor(Integer patientId, Integer doctorId){
		Account patient = (Account) mediInterface.get(Account.class, patientId);
		
		String patientEmail = patient.geteMail();
		Account doctor = (Account) mediInterface.get(Account.class, doctorId);
		String doctorEmail = doctor.geteMail();
		
		EmailService emailService = new EmailServiceImpl();
		emailService.sendMail(patientEmail, doctorEmail, "localhost", "New Patient Request", 
				"A new patient has requested for you to be under his or her account! /n"
				+"https://localhost:7002/medi/provider/profile"
				);
		
	}
	
	public Boolean approvePatient(Integer patientId, Integer providerId) {
		return mediInterface.approveRequest(patientId, providerId);
	}

	public Boolean denyPatient(Integer patientId, Integer providerId) {
		return mediInterface.denyRequest(patientId, providerId);
	}
	
	private List<Account> getNonePersistentAccounts(List<Account> accounts){
		List<Account> proxyAccounts = new ArrayList<Account>(); 
		for(int i = 0; i < accounts.size(); i++ ){
			proxyAccounts.add( getNonePersistentAccount(accounts.get(i)) );
		}
		return proxyAccounts;
	}
	
	private Account getNonePersistentAccount( Account account ){
		Account newAccount = new Account();
		newAccount.setId(account.getId());
		newAccount.setUsername(account.getUsername());
		newAccount.setFirstName(account.getFirstName());
		newAccount.setLastName(account.getLastName());
		newAccount.seteMail(account.geteMail());
		newAccount.setAddress(account.getAddress());
		return newAccount;
	}
	
	/**
	 * 
	 */
	public Account createPatientAccount(CreatePatientForm patientForm) {
		Account acct = new Account();
		Address address = new Address();
		
		address.setAddressLine1(patientForm.getAddress1());
		address.setAddressLine2(patientForm.getAddress2());
		address.setCity(patientForm.getCity());
		address.setPhoneNumber(patientForm.getPhone());
		address.setZip(patientForm.getZip());
		address.setState( (State) mediInterface.get(State.class, Integer.parseInt(patientForm.getState())));
		
		acct.setUsername(patientForm.getUsername());
		acct.setPassword( bCryptPasswordEncoder.encode(patientForm.getPassword()) );
		acct.setFirstName(patientForm.getFirstName());
		acct.setLastName(patientForm.getLastName());
		acct.seteMail(patientForm.getEmail());
		acct.setType( (AccountType) mediInterface.get(AccountType.class, 2));
		acct.setSocialSecurity( patientForm.getSocialSecurity() );
		
		DateFormat df = new SimpleDateFormat("dd-MM-yy"); 
		Date startDate = null;
		try {
			startDate = df.parse(patientForm.getDateOfBirth());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		acct.setDob( startDate );
		acct.setSex(patientForm.getSex().charAt(0));
		
		acct.setAddress(address);

		if( ! mediInterface.checkExist(acct) ){
			acct.setSocialSecurity(bCryptPasswordEncoder.encode(acct.getSocialSecurity()));
			mediInterface.save(acct);
		}
		
		acct.setPassword("");
		acct.setSocialSecurity("");
		return acct;
	}


	public String routToProperPath(Account account) throws Exception {
		String accountType = account.getType().getDescription();
		if( accountType.equals("PATIENT") )
			return "patient/profile";
		else if(accountType.equals("NURSE") )
			return "assistant/profile";
		else if(accountType.equals("DOCTOR") )
			return "provider/profile";
		else
			throw new Exception("User Type is Invalid");
	}

	public List<State> getStates() {
		return (List<State>) mediInterface.getList(State.class);
	}


	public Account editProfile(EditProfile editProfile) {
		//get the username first
		boolean modified = false;
		String username = editProfile.getUsername();
		Account account = mediInterface.getAccount(username);
		
		if( editProfile.getEmail() != null ){
			account.seteMail(editProfile.getEmail());
			modified = true;
		}
		
		if( editProfile.getUsername() != null ){
			account.setUsername(editProfile.getUsername());
			modified = true;
		}
		
		Address address = account.getAddress();
		if( editProfile.getStreet() != null ){
			address.setAddressLine1(editProfile.getStreet());
			modified = true;
		}
		
		if( editProfile.getCity() != null ){
			address.setCity(editProfile.getCity());
			modified = true;
		}
		
		if( editProfile.getZip() != null){
			address.setZip(editProfile.getZip());
			modified = true;
		}
		
		if( editProfile.getState() != address.getState().getId() ){
			State newState = (State) mediInterface.get(State.class, editProfile.getState());
			address.setState(newState);
			modified = true;
		}
		mediInterface.update(account);
		return account;
	}

	public List<Account> getDoctorsPatients(Integer doctorId) {
		return (List<Account>) mediInterface.getList(Account.class);
	}

	public List<Record> getPatientRecords(Integer patientId) {
		List<Record> records = mediInterface.getFiles(patientId);
		return records;
	}
	
	public Boolean updateRecords(Integer patientId, String key, String type) {
		return mediInterface.insertFile(key, type, patientId);
	}

	public List<DocumentObject> generateDocumentObjects(List<Record> records, List<String> publicList) {
		List<DocumentObject> result = new ArrayList<DocumentObject>();
		for( int i = 0; i < records.size(); i++){
			Record cur = records.get(i);
			
			result.add(new DocumentObject(cur.getType(), publicList.get(i), cur.getFilename()) );
		}
		return result;
	}

	public List<EvaluationObject> getPatientEvaluations(Integer patientId) {
		Account account = (Account) mediInterface.get(Account.class, patientId);
		String patientName = account.getFirstName()+" "+account.getLastName();
		List<Appointment> appointments = mediInterface.getPatientAppointments(patientId);
		List<EvaluationObject> appointmentList = new ArrayList<EvaluationObject>();
		for(int i = 0; i < appointments.size(); i++){
			Appointment cur = appointments.get(i);
			String doctorName = "";
			PatientEvaluation persistedEval = cur.getEvaluation();
			PatientEvaluationForm nonePersistedEval = new PatientEvaluationForm();
	
			nonePersistedEval.setAllergies(persistedEval.getAllergies());
			nonePersistedEval.setBloodPressure(persistedEval.getBloodPressure());
			nonePersistedEval.setBloodSugar(persistedEval.getBloodSugar());
			nonePersistedEval.setBodyTemp(persistedEval.getBodyTemp());
			nonePersistedEval.setHeartRate(persistedEval.getHeartRate());
			nonePersistedEval.setHeight(persistedEval.getHeight());
			nonePersistedEval.setMedications(persistedEval.getMedications());
			nonePersistedEval.setPriorIssues(persistedEval.getPriorIssues());
			nonePersistedEval.setReason(persistedEval.getReason());
			nonePersistedEval.setSymptoms(persistedEval.getSymptoms());
			nonePersistedEval.setWeight(persistedEval.getWeight());
			nonePersistedEval.setPatientId(patientId);
					
			appointmentList.add(new EvaluationObject(cur.getId(), patientName, cur.getTime(), doctorName, nonePersistedEval ));
		}
		return appointmentList;
	}

	public Map<Integer, ArrayList<? extends Number>> getBloodPressure(Integer patientId) {
		List<Appointment> appointments = mediInterface.getPatientAppointments(patientId);
		
		Map<Integer, ArrayList<? extends Number>> mapList = new HashMap<Integer, ArrayList<? extends Number>>();
		ArrayList<Long> xAxis = new ArrayList<Long>();
		ArrayList<Integer> systolicList = new ArrayList<Integer>();
		ArrayList<Integer> diastolicList = new ArrayList<Integer>();
		
		for( Appointment app: appointments)
		{
			
				String bloodPressure = app.getEvaluation().getBloodPressure();
				Integer systolic = null;
				Integer diastolic = null;
				if( bloodPressure != null && !bloodPressure.isEmpty())
				{
					String[] split = bloodPressure.split("/");
					
					try
					{
						systolic = Integer.parseInt(split[0].trim());
						diastolic = Integer.parseInt(split[1].trim());
					}
					catch(NumberFormatException e)
					{
						continue;
					}
					
					
					Long x = app.getTime().getTime();
					
					xAxis.add(x);
					systolicList.add(systolic);
					diastolicList.add(diastolic);
				}
		}
		mapList.put(0, xAxis);
		mapList.put(1, systolicList);
		mapList.put(2, diastolicList);
		return mapList;
	}

	public Map<Integer, ArrayList<? extends Number>> getWeight(Integer patientId) 
	{
		List<Appointment> appointments = mediInterface.getPatientAppointments(patientId);
		
		Map<Integer, ArrayList<? extends Number>> mapList = new HashMap<Integer, ArrayList<? extends Number>>();
		ArrayList<Long> xAxis = new ArrayList<Long>();
		ArrayList<Double> weight = new ArrayList<Double>();
		
		for( Appointment app: appointments)
		{
			
				Double apptWeight = app.getEvaluation().getWeight();
				if( apptWeight != null )
				{
					
					
					Long x = app.getTime().getTime();
					
					xAxis.add(x);
					weight.add(apptWeight);
				}
		}
		mapList.put(0, xAxis);
		mapList.put(1, weight);
		return mapList;
	}
	
	public Map<Integer, ArrayList<? extends Number>> getHeartRate(Integer patientId) 
	{
		List<Appointment> appointments = mediInterface.getPatientAppointments(patientId);
		
		Map<Integer, ArrayList<? extends Number>> mapList = new HashMap<Integer, ArrayList<? extends Number>>();
		ArrayList<Long> xAxis = new ArrayList<Long>();
		ArrayList<Integer> heartRate = new ArrayList<Integer>();
		
		for( Appointment app: appointments)
		{
			
				Integer apptHeartRate = app.getEvaluation().getHeartRate();
				if( apptHeartRate != null )
				{
					
					
					Long x = app.getTime().getTime();
					
					xAxis.add(x);
					heartRate.add(apptHeartRate);
				}
		}
		mapList.put(0, xAxis);
		mapList.put(1, heartRate);
		return mapList;
	}
	
	public Map<Integer, ArrayList<? extends Number>> getBloodSugar(Integer patientId) 
	{
		List<Appointment> appointments = mediInterface.getPatientAppointments(patientId);
		
		Map<Integer, ArrayList<? extends Number>> mapList = new HashMap<Integer, ArrayList<? extends Number>>();
		ArrayList<Long> xAxis = new ArrayList<Long>();
		ArrayList<Double> bloodSugar = new ArrayList<Double>();
		
		for( Appointment app: appointments)
		{
			
				Double apptSugar = app.getEvaluation().getBloodSugar();
				if( apptSugar != null )
				{
					
					
					Long x = app.getTime().getTime();
					
					xAxis.add(x);
					bloodSugar.add(apptSugar);
				}
		}
		mapList.put(0, xAxis);
		mapList.put(1, bloodSugar);
		return mapList;
	}

	

}

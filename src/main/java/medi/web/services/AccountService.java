package medi.web.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import entities.Account;
import entities.Record;
import entities.State;
import medi.web.form.CreatePatientForm;
import medi.web.form.EditProfile;
import medi.web.form.EvaluationObject;

public interface AccountService {

	public Account getAccountByName( String username );
	
	public Account createPatientAccount( CreatePatientForm patientForm ); //will check for existing social security
	
	public String routToProperPath(Account account) throws Exception;

	public List<Account> getAvailableProvidersByUserID(Integer userID);
	
	public List<Account> getCurrentProvidersByUserID(Integer userID);
	
	public List<Account> getDoctorsPatients(Integer doctorId);
	
	public List<?> getDocuments( String username, String type ) throws Exception;
	
	public List<State> getStates();

	public Account editProfile(EditProfile editProfile);

	public List<Account> getPendingPatientByProviderId(Integer providerId);
	
	public List<Account> getPendingProvidersByPatientId(Integer patientId);
	
	public List<Account> getActivePatientsByProviderID(Integer providerId);
	
	public Boolean approvePatient(Integer patientId, Integer providerId);
	
	public Boolean denyPatient(Integer patientId, Integer providerId);
	
	public List<Record> getPatientRecords(Integer patientId);

	public Boolean updateRecords(Integer patientId, String key, String type);

	public List<DocumentObject> generateDocumentObjects(List<Record> records, List<String> publicList);

	public List<EvaluationObject> getPatientEvaluations(Integer patientId);

	public Map<Integer, ArrayList<? extends Number> > getBloodPressure(Integer patientId);
	public Map<Integer, ArrayList<? extends Number> > getWeight(Integer patientId);

	public List<Account> getNursePatients(Integer nurseId);

	public List<Account> searchProviderMatch(String providerMatch, Integer patientId);

	public Map<Integer, ArrayList<? extends Number>> getBloodSugar(Integer patientId);

	public Map<Integer, ArrayList<? extends Number>> getHeartRate(Integer patientId);
	
	public List<Account> searchPatientMatch(String patientMatch, Integer doctorId);

	public void subScribeToDoctor(Integer patientId, Integer doctorId);
}

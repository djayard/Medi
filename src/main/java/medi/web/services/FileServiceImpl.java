package medi.web.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jets3t.service.S3Service;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.acl.GroupGrantee;
import org.jets3t.service.acl.Permission;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.springframework.web.multipart.MultipartFile;

import entities.Record;

/**
 * 
 * @author Denny
 *
 */
public class FileServiceImpl implements FileService{

	private static AWSCredentials credentials;
	private static S3Service s3;
	private final static String BUCKET = "revature.medi";
	static
	{
		credentials = new AWSCredentials("AKIAJI4VI46ZZECMG4BA", "DqPMrj5EtCGqHpVFsFXrO65yWSB6cm5nC2MmGIkY");
		s3 = new RestS3Service(credentials);
	}
	

	public boolean upload(MultipartFile file)
	{
		return uploadFile(file,file.getOriginalFilename());
	}
	
	public String upload(Integer patientId, MultipartFile file) 
	{
		if( file.getName() == null || file.getName().isEmpty())
			return "";
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yy-kk:mm");
		String prefix = patientId + "." + fmt.format(Calendar.getInstance().getTime()) + ".";
		String filename = prefix + file.getOriginalFilename();
		
		uploadFile(file,filename);
		
		return filename;
	}
	
	public Map<String, String> getFileLinks(Integer patientId) 
	{
		return getLinks(patientId.toString());
	}
	
	private boolean uploadFile(MultipartFile file, String filename)
	{
		try{
		S3Bucket bucket = s3.getBucket(BUCKET);
		S3Object image = new S3Object(filename);
		image.setDataInputStream(file.getInputStream());
		image.setContentLength(file.getSize());
		image.setContentType(file.getContentType());
		AccessControlList acl = new AccessControlList();
		acl.setOwner(bucket.getOwner());
		acl.grantPermission(GroupGrantee.AUTHENTICATED_USERS, Permission.PERMISSION_READ);
		image.setAcl(acl);
		
		s3.putObject(bucket, image);
		
		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}	
		
		return true;
	}
	
	private Map<String,String> getLinks(String prefix)
	{
		
		Map<String,String> result = new HashMap<String,String>();
		Calendar startTime = Calendar.getInstance();
		startTime.add(Calendar.MINUTE, 10);
		Date expireTime = startTime.getTime();
		
		try
		{
			S3Object[] files = s3.listObjects(BUCKET, prefix, null);
			for(S3Object file : files)
			{
				result.put(file.getKey(), s3.createSignedGetUrl(BUCKET, file.getKey(), expireTime));
			}
		
		}catch(Exception e)
		{
			e.printStackTrace();
			result.clear();
			return result;
		}
		
		
		return result;
	}

	public List<String> getFileLinks(List<Record> records) {
		
		List<String> links = new ArrayList<String>(records.size());
		
		Calendar startTime = Calendar.getInstance();
		startTime.add(Calendar.MINUTE, 10);
		Date expireTime = startTime.getTime();
		
		try
		{
			for(Record record : records)
			{
				links.add(s3.createSignedGetUrl(BUCKET, record.getFilename(), expireTime));
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			links.clear();
		}
		
		return links;
		
	}
	
	public static void main(String[] args )
	{
		FileServiceImpl service = new FileServiceImpl();
		
		Map<String,String> links = service.getLinks("13");
		
		links.keySet();
		
	}

	

	


	
	
	/*
	@RequestMapping(path="download")
	public String download(@RequestParam("downloadFileSelect") String filename)
	{
		try{
		S3Object file = s3.getObject("revature.medi", filename);
		
		BufferedInputStream in = new BufferedInputStream(file.getDataInputStream());

		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream("C:/Users/Denny/Desktop/2" + filename));
		
		byte[] buffer = new byte[1024];
		
		int bytesRead = 0;
		while( (bytesRead = in.read(buffer, 0, 1024)) != -1 )
		{
			out.write(buffer,0,bytesRead);
		}
		
		out.flush();
		out.close();
		
		}catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
		return "success";
	}
	
	private String[] files()
	{
		try{
			S3Object objects[] =  s3.listObjects("revature.medi");
			String files[] = new String [objects.length];
			for(int i = 0; i < objects.length; ++i)
			{
				files[i] = objects[i].getKey();
			}
			
			return files;
	
		}catch(Exception e)
		{
			return null;
		}
	}*/
}

package medi.web.services;

public interface EmailService {
	public boolean sendMail(String from, String to, String host, String subject, String message);
}

package medi.web.services;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import entities.Record;

public interface FileService {
	
	public boolean upload(MultipartFile file);
	
	public String upload(Integer patientId, MultipartFile file);
	public Map<String,String> getFileLinks(Integer patientId);
	public List<String> getFileLinks(List<Record> records);
	
	

}

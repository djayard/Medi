package medi.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import dao.MediInterface;
import entities.Account;

//This will not be included as a spring bean
public class AccountSecurity implements UserDetailsService {

	private final MediInterface mediInterface;
	
	public AccountSecurity( MediInterface mediInterface ){
		this.mediInterface = mediInterface;
	}

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = mediInterface.getAccount(username);
		if(account == null)
			System.out.println("account not found");
			
		if(account != null ){
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			//we need to append the "ROLE_" prefix
			authorities.add(new SimpleGrantedAuthority("ROLE_"+account.getType().getDescription()) );
			System.out.println("authorities found "+account.getType().getDescription());
			
			return new User( account.getUsername(), account.getPassword(), authorities );
		}else{
			throw new UsernameNotFoundException("User '"+username+"' not found ");
		}
	}
		
}

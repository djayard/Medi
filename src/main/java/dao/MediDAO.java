package dao; 

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import entities.*;
import entities.TestLine.TestLineId;

public class MediDAO implements MediInterface {
	
	private SessionFactory sf;
	
	private PasswordEncoder encoder;
	
	@Autowired
	private void setPasswordEncoder(PasswordEncoder encoder)
	{
		this.encoder = encoder;
	}
	
	@Transactional
	public boolean save(Object obj)
	{
		Session session = sf.getCurrentSession();
		session.save(obj);
		return true;
	}
	
	@Transactional
	public boolean update(Object obj)
	{
		Session session = sf.getCurrentSession();
		session.update(obj);
		return true;
	}
	
	@Transactional
	public boolean delete(Class<?> objClass, Integer id )
	{
		Session session = sf.getCurrentSession();
		session.delete( session.get(objClass, id));
		return true;
	}
	
	@Transactional
	public Object get(Class<?> objClass, Integer id)
	{
		Session session = sf.getCurrentSession();
		return session.get(objClass, id);
	}
	
	@Transactional
	public java.util.List<?> getList(java.lang.Class<?> targetClass)
	{
		String query = "from " + targetClass.getName() + " order by id";
		
		Session session = sf.getCurrentSession();
		
		return session.createQuery(query).list();
	}
	
	@Transactional
	public Account getAccount(String username, String password)
	{
		Session session = sf.getCurrentSession();
		Account acct = (Account) session.
				createQuery("from entities.Account where username = :username and password = :password").
				setString("username", username).
				setString("password", password).
				uniqueResult();
		return acct;
	}
	
	@Transactional
	public Account getAccount(String username) {
		Session session = sf.getCurrentSession();
		Account acct = (Account) session.
				createQuery("from entities.Account where username = :username").
				setString("username", username).
				uniqueResult();
		return acct;
	}
			
	@Transactional
	public Account getAccountByName(String username)
	{
		Session session = sf.getCurrentSession();
		Account acct = (Account) session.
				createQuery("from entities.Account where username = :username ").
				setString("username", username).
				uniqueResult();
		return acct;
	}
	
	@Transactional
	public boolean checkExist(Account acct)
	{
		Session session = sf.getCurrentSession();
		
		if( session.createQuery("from entities.Account where username = :username")
				.setString("username", acct.getUsername())
				.uniqueResult() != null ? true : false 
				)
			return true;
		
		List<String> ssns = session.createQuery("select socialSecurity from entities.Account").list();
		for(String code: ssns)
		{
			if( code != null && !code.isEmpty())
			if( encoder.matches(acct.getSocialSecurity(), code) )
				return true;
		}
		
		return false;
	}
	
	@Transactional
	public boolean addDoctor(Integer patient, Integer doctor)
	{
		Session session = sf.getCurrentSession();
		
		((Account) session.get(Account.class, patient) ).
			getDoctors().add((Account) session.get(Account.class, doctor));
		
		return true;
	}
	
	@Transactional
	public boolean removeDoctor(Integer patient, Integer doctor)
	{
		Session session = sf.getCurrentSession();
		
		Account doc = (Account) session.get(Account.class, doctor);
		Account pat = (Account) session.get(Account.class, patient);
		
		pat.getDoctors().remove(doc);
		
		return true;
	}
	
	@Transactional
	public boolean addSupport(Integer doctor, Integer support)
	{
		Session session = sf.getCurrentSession();
		
		((Account) session.get(Account.class, doctor) ).
		getSupportStaff().add((Account) session.get(Account.class, support));
		
		return true;
	}
	
	@Transactional
	public java.util.List<Account> getDoctors()
	{
		Session session = sf.getCurrentSession();
		
		List<Account> doctors;
		
		doctors = session.createQuery("from entities.Account where type.id = 1 ").list();
		
		return doctors;
	}
	
	@Transactional
	public java.util.List<Account> getUnusedDoctors(Integer id)
	{
		Session session = sf.getCurrentSession();
		
		Account patient = (Account) session.get(Account.class, id);
		
		List<Account> doctors = session.
				createQuery("from entities.Account where type.id = 1 ").list();
		
		List<Account> patientDocs = patient.getDoctors();
	
		for(Account doctor: patientDocs)
			doctors.remove(doctor);
		
		return doctors;
	}
	
	@Transactional
	public java.util.List<Account> getPatientsDoctors(Integer id)
	{
		Session session = sf.getCurrentSession();
		
		List<Account> doctors = session.
				createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_ID in "
						+ "(select DOCTOR_ID from DOCTORS_PATIENTS where PATIENT_ID = ? and status = 'APPROVED' ) ")
				.addEntity(Account.class)
				.setInteger(0, id).list();
		
		return doctors;
	}
	
	public SessionFactory getSf() {
		return sf;
	}

	public void setSf(SessionFactory sf) {
		this.sf = sf;
	}

	@Transactional
	public List<Account> getNursesPatients(Integer nurseId) {
		
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_ID in "
				+ "(select PATIENT_ID from DOCTORS_PATIENTS where STATUS = 'APPROVED' "
				+ "and DOCTOR_ID in (select DOCTOR_ID from DOCTORS_SUPPORT where SUPPORT_ID "
				+ "= ? ))").addEntity(Account.class).setInteger(0, nurseId).list();
		
	
		
	}

	@Transactional
	public boolean createAppointment(PatientEvaluation eval, Integer patientId) {
		
		Session session = sf.getCurrentSession();
		Account patient = (Account) session.get(Account.class, patientId);
		
		Appointment appt = new Appointment();
		eval.setPatient(patient);
		appt.setPatient(patient);
		appt.setTime(new Date());
		
		session.save(eval);
		appt.setEvaluation( eval );
		session.save(appt);
		
		return true;
	}

	@Transactional
	public List<Appointment> getPatientAppointments(Integer patientId) {
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from APPOINTMENTS where PATIENT_ID  = ? "
				+ "order by APPOINTMENT_ID ASC").addEntity(Appointment.class)
				.setInteger(0, patientId).list();
	}

	@Transactional
	public List<Appointment> getDoctorsPatientsAppointments(Integer nurseId) {
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from APPOINTMENTS where DOCTOR_ID is not null and PATIENT_ID in "
				+ "(select PATIENT_ID from DOCTORS_PATIENTS where DOCTOR_ID in "
				+ "(select DOCTOR_ID from DOCTORS_SUPPORT where SUPPORT_ID = ? )) order by APPOINTMENT_ID DESC")
				.addEntity(Appointment.class).setInteger(0, nurseId).list();
	}

	@Transactional
	public Test getTests(Integer patientId) {
		
		Session session = sf.getCurrentSession();
		
		Test test = (Test) session.createQuery("from entities.Test where patient.id = :patient")
				.setInteger("patient", patientId).uniqueResult();
		
		
		if( test == null )
		{
			test = new Test();
			test.setPatient( (Account) session.get(Account.class, patientId) );
			session.save(test);
		}
		
		List<TestLine> lines = test.getLines();
		if( lines != null )
			Hibernate.initialize(lines);
		else
			test.setLines(new ArrayList<TestLine>());
		
		return  test;
	}
	
	@Transactional
	public boolean updateTest(Integer patientId, TestLine line) 
	{
		Session session = sf.getCurrentSession();
		
		Test test = (Test) session.createQuery("from entities.Test where patient.id = :patient")
				.setInteger("patient", patientId).uniqueResult();
		
		List<TestLine> lines = test.getLines();
		int lineNum = 1;
		if( lines != null )
			lineNum = lines.size() + 1;
		
		line.setId(new TestLineId());
		line.getId().setTest(test);
		line.getId().setLineId(lineNum);
		
		session.save(line);
		
		return true;
	}
	
	@Transactional
	public boolean createNewRequest(Integer patient, Integer doctor) {
	
		Session session = sf.getCurrentSession();
		
		session.createSQLQuery("insert into DOCTORS_PATIENTS (PATIENT_ID, DOCTOR_ID, STATUS) "
				+ "values (?,?,'PENDING')").setInteger(0,patient).setInteger(1, doctor).executeUpdate();
		return true;
	}
	
	
	
	
	@Transactional
	public List<Account> getPendingRequests(Integer doctor) 
	{
		Session session = sf.getCurrentSession();
		
		List<Account> pending =	session.createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_ID in "
				+ "(select PATIENT_ID from DOCTORS_PATIENTS where doctor_id = ? and status = 'PENDING' )")
				.addEntity(Account.class).setInteger(0, doctor).list();
		
		
		return pending;
	}
	
	@Transactional
	public List<Account> patientGetPendingRequests(Integer patient) {
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_ID in "
				+ "(select DOCTOR_ID from DOCTORS_PATIENTS where PATIENT_ID = ? and "
				+ "STATUS = 'PENDING' )")
		.addEntity(Account.class).setInteger(0, patient).list();
	}

	@Transactional
	public boolean approveRequest(Integer patient, Integer doctor) {
	
		Session session = sf.getCurrentSession();
		
		session.createSQLQuery("update DOCTORS_PATIENTS set status = 'APPROVED' "
				+ "where PATIENT_ID = :patient and DOCTOR_ID = :doctor ")
		.setInteger("patient", patient)
		.setInteger("doctor", doctor).executeUpdate();
		
		return true;
	}

	@Transactional
	public boolean denyRequest(Integer patient, Integer doctor) {
		Session session = sf.getCurrentSession();
		
		session.createSQLQuery("update DOCTORS_PATIENTS set status = 'DENIED' "
				+ "where PATIENT_ID = :patient and DOCTOR_ID = :doctor ")
		.setInteger("patient", patient)
		.setInteger("doctor", doctor).executeUpdate();
		
		return true;
	}

	@Transactional
	public List<Account> getDoctorsPatients(Integer doctorId) {
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_ID in "
				+ "(select PATIENT_ID from DOCTORS_PATIENTS where DOCTOR_ID = ? and STATUS = 'APPROVED' )")
		.addEntity(Account.class).setInteger(0, doctorId).list();
	}

	@Transactional
	public boolean insertFile(String filename, String type, Integer patientId) {
		Session session = sf.getCurrentSession();
		
		Record record = new Record();
		record.setFilename(filename);
		record.setType(type);
		record.setPatient((Account)session.get(Account.class,patientId));
		
		session.save(record);
		
		return true;
		
	}

	@Transactional
	public List<Record> getFiles(Integer patient) {
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from RECORDS where PATIENT_ID = ? order by RECORD_ID DESC")
				.addEntity(Record.class).setInteger(0, patient).list();
	}

	@Transactional
	public List<Account> findDoctor(String pattern, Integer patientId) 
	{
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_TYPE = 1 and "
				+ "ACCOUNT_ID not in "
				+ "(select DOCTOR_ID from DOCTORS_PATIENTS where PATIENT_ID = :patient ) "
				+ "and LAST_NAME like :pattern ").addEntity(Account.class)
				.setString("pattern", pattern + "%").setInteger("patient", patientId).list();
	}

	@Transactional
	public List<Account> findPatient(String pattern, Integer doctorId) 
	{
		Session session = sf.getCurrentSession();
		
		return session.createSQLQuery("select * from USER_ACCOUNTS where ACCOUNT_TYPE = 2 and "
				+ "ACCOUNT_ID in "
				+ "(select PATIENT_ID from DOCTORS_PATIENTS where DOCTOR_ID = :doctor and STATUS = 'APPROVED' ) "
				+ "and LAST_NAME like :pattern ").addEntity(Account.class)
				.setString("pattern", pattern + "%").setInteger("doctor", doctorId).list();
	}
	

}

package dao;

import java.util.List;

import entities.*;

public interface MediInterface {
	
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Class<?> objClass, Integer i);
	public Object get(Class<?> objClass, Integer i);

	public Account getAccount(String username, String password);
	public Account getAccount(String username);
	public Account getAccountByName(String username);
	public boolean checkExist(Account acct);

	
	public boolean addDoctor(Integer patient, Integer doctor);
	public boolean createNewRequest(Integer patient, Integer doctor);
	public List<Account> getPendingRequests(Integer doctor);
	public List<Account> patientGetPendingRequests(Integer patient);
	
	public boolean addSupport(Integer doctor, Integer support);
	public boolean removeDoctor(Integer patient, Integer doctor);
	
	public List<Account> getDoctors();
	public List<Account> getDoctorsPatients(Integer doctorId);
	public List<Account> getUnusedDoctors(Integer patientId);
	public List<Account> getPatientsDoctors(Integer patientId);
	
	public List<?> getList(Class<?> targetClass);
	public List<Account> getNursesPatients(Integer nurseId);
	
	public boolean createAppointment(PatientEvaluation eval, Integer patientId);
	public List<Appointment> getPatientAppointments(Integer patientId);
	public List<Appointment> getDoctorsPatientsAppointments(Integer nurseId);
	
	public Test getTests(Integer patientId);
	public boolean updateTest(Integer patientId, TestLine line);
	
	public boolean insertFile(String filename, String type, Integer patientId);
	public List<Record> getFiles(Integer patient);
	
	public boolean approveRequest(Integer patient, Integer doctor);
	public boolean denyRequest(Integer patient, Integer doctor);
	
	public List<Account> findDoctor(String pattern, Integer patientId);
	
	public List<Account> findPatient(String pattern, Integer doctorId);
	
}

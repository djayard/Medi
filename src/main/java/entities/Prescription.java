package entities;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="PRESCRIPTIONS")
public class Prescription {

	@Id
	@SequenceGenerator(name="prescriptGen", sequenceName="PRESCRIPTION_SEQ")
	@GeneratedValue(generator="prescriptGen", strategy=GenerationType.AUTO)
	@Column(name="PRESCRIPTION_ID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="DOCTOR_ID")
	private Account doctor;
	@ManyToOne
	@JoinColumn(name="PATIENT_ID")
	private Account patient;
	@Column(name="DATE_ORDERED")
	private Date orderDate;
	
	@OneToMany(mappedBy="id.prescript")
	private List<PrescriptionLine> lines;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Account getDoctor() {
		return doctor;
	}
	public void setDoctor(Account doctor) {
		this.doctor = doctor;
	}
	public Account getPatient() {
		return patient;
	}
	public void setPatient(Account patient) {
		this.patient = patient;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	
	
	public List<PrescriptionLine> getLines() {
		return lines;
	}
	public void setLines(List<PrescriptionLine> lines) {
		this.lines = lines;
	}
	@Override
	public String toString() {
		return "Prescription [id=" + id + ", doctor=" + doctor + ", patient=" + patient + ", orderDate=" + orderDate
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doctor == null) ? 0 : doctor.hashCode());
		result = prime * result + ((orderDate == null) ? 0 : orderDate.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prescription other = (Prescription) obj;
		if (doctor == null) {
			if (other.doctor != null)
				return false;
		} else if (!doctor.equals(other.doctor))
			return false;
		if (orderDate == null) {
			if (other.orderDate != null)
				return false;
		} else if (!orderDate.equals(other.orderDate))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		return true;
	}
	
	
}

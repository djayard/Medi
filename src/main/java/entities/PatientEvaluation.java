package entities;

import javax.persistence.*;

@Entity
@Table(name="PATIENT_EVALUATIONS")
public class PatientEvaluation {
	
	@Id
	@SequenceGenerator(name="evalGen", sequenceName="PATIENT_EVAL_SEQ")
	@GeneratedValue(generator="evalGen", strategy=GenerationType.AUTO)
	@Column(name="EVAL_ID")
	private Integer id;
	
	@Column(name="BODY_TEMP")
	private Double bodyTemp;
	@Column(name="BLOOD_PRESSURE")
	private String bloodPressure;
	@Column(name="HEART_RATE")
	private Integer heartRate;
	@Column(name="BLOOD_SUGAR")
	private Double bloodSugar;
	@Column(name="WEIGHT")
	private Double weight;
	@Column(name="HEIGHT")
	private String height;
	@Column(name="REASON")
	private String reason;
	@Column(name="SYMPTOMS")
	private String symptoms;
	@Column(name="ALLERGIES")
	private String allergies;
	@Column(name="MEDICATIONS")
	private String medications;
	@Column(name="PRIOR_ISSUES")
	private String priorIssues;
	@ManyToOne
	@JoinColumn(name="PATIENT_ID")
	private Account patient;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getBodyTemp() {
		return bodyTemp;
	}
	public void setBodyTemp(Double bodyTemp) {
		this.bodyTemp = bodyTemp;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSymptoms() {
		return symptoms;
	}
	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}
	public String getAllergies() {
		return allergies;
	}
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	public String getMedications() {
		return medications;
	}
	public void setMedications(String medications) {
		this.medications = medications;
	}
	public String getPriorIssues() {
		return priorIssues;
	}
	public void setPriorIssues(String priorIssues) {
		this.priorIssues = priorIssues;
	}
	public Account getPatient() {
		return patient;
	}
	public void setPatient(Account patient) {
		this.patient = patient;
	}
	
	public Integer getHeartRate() {
		return heartRate;
	}
	public void setHeartRate(Integer heartRate) {
		this.heartRate = heartRate;
	}
	public Double getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(Double bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	@Override
	public String toString() {
		return "PatientEvaluation [id=" + id + ", bodyTemp=" + bodyTemp + ", bloodPressure=" + bloodPressure
				+ ", weight=" + weight + ", height=" + height + ", reason=" + reason + ", symptoms=" + symptoms
				+ ", allergies=" + allergies + ", medications=" + medications + ", priorIssues=" + priorIssues
				+ ", patient=" + patient + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allergies == null) ? 0 : allergies.hashCode());
		result = prime * result + ((bloodPressure == null) ? 0 : bloodPressure.hashCode());
		result = prime * result + ((bodyTemp == null) ? 0 : bodyTemp.hashCode());
		result = prime * result + ((height == null) ? 0 : height.hashCode());
		result = prime * result + ((medications == null) ? 0 : medications.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + ((priorIssues == null) ? 0 : priorIssues.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result + ((symptoms == null) ? 0 : symptoms.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatientEvaluation other = (PatientEvaluation) obj;
		if (allergies == null) {
			if (other.allergies != null)
				return false;
		} else if (!allergies.equals(other.allergies))
			return false;
		if (bloodPressure == null) {
			if (other.bloodPressure != null)
				return false;
		} else if (!bloodPressure.equals(other.bloodPressure))
			return false;
		if (bodyTemp == null) {
			if (other.bodyTemp != null)
				return false;
		} else if (!bodyTemp.equals(other.bodyTemp))
			return false;
		if (height == null) {
			if (other.height != null)
				return false;
		} else if (!height.equals(other.height))
			return false;
		if (medications == null) {
			if (other.medications != null)
				return false;
		} else if (!medications.equals(other.medications))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (priorIssues == null) {
			if (other.priorIssues != null)
				return false;
		} else if (!priorIssues.equals(other.priorIssues))
			return false;
		if (reason == null) {
			if (other.reason != null)
				return false;
		} else if (!reason.equals(other.reason))
			return false;
		if (symptoms == null) {
			if (other.symptoms != null)
				return false;
		} else if (!symptoms.equals(other.symptoms))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}

}

package entities;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="TESTS")
public class Test {
	
	@Id
	@SequenceGenerator(name="testGen", sequenceName="TEST_SEQ")
	@GeneratedValue(generator="testGen", strategy=GenerationType.AUTO)
	@Column(name="TEST_ID")
	private Integer id;
	
	@ManyToOne()
	@JoinColumn(name="DOCTOR_ID")
	private Account doctor;
	
	@ManyToOne()
	@JoinColumn(name="PATIENT_ID")
	private Account patient;
	
	@Column(name="DATE_ORDERED")
	private Date dateOrdered;
	
	@OneToMany(mappedBy="id.test")
	private List<TestLine> lines;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Account getDoctor() {
		return doctor;
	}

	public void setDoctor(Account doctor) {
		this.doctor = doctor;
	}

	public Account getPatient() {
		return patient;
	}

	public void setPatient(Account patient) {
		this.patient = patient;
	}

	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}


	public List<TestLine> getLines() {
		return lines;
	}

	public void setLines(List<TestLine> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		return "Test [id=" + id + ", doctor=" + doctor + ", patient=" + patient + ", dateOrdered=" + dateOrdered + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOrdered == null) ? 0 : dateOrdered.hashCode());
		result = prime * result + ((doctor == null) ? 0 : doctor.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		if (dateOrdered == null) {
			if (other.dateOrdered != null)
				return false;
		} else if (!dateOrdered.equals(other.dateOrdered))
			return false;
		if (doctor == null) {
			if (other.doctor != null)
				return false;
		} else if (!doctor.equals(other.doctor))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		return true;
	}
	
	
}

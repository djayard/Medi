package entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="USER_ACCOUNTS")
public class Account implements Comparable<Account> {
	
	@Id
	@Column(name="ACCOUNT_ID")
	@SequenceGenerator(name="acctGen", sequenceName="USER_ACCT_SEQ")
	@GeneratedValue(generator="acctGen", strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="USER_PASSWORD")
	private String password;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="SSN")
	private String socialSecurity;
	
	@Column(name="SEX")
	private Character sex;
	
	@Column(name="DOB")
	private Date dob;
	
	@Column(name="E_MAIL")
	private String eMail;
	
	@Column(name="SPECIAL")
	private String special; 
	
	@ManyToOne
	@Cascade(value=CascadeType.SAVE_UPDATE)
	@JoinColumn(name="ADDRESS")
	private Address address;
	
	@Column(name="MEDICAL_GROUP_ID")
	private Integer groupId;
	
	@ManyToOne
	@JoinColumn(name="ACCOUNT_TYPE")
	private AccountType type;
	
	/*
	 * From the JPA 2.0 spec, the defaults are
	OneToMany: LAZY
	ManyToOne: EAGER
	ManyToMany: LAZY
	OneToOne: EAGER
	Columns : EAGER
	*/
	@OneToMany(mappedBy="patient")
	private List<ImmunRecord> immunRecord;
	
	@OneToMany(mappedBy="patient")
	private List<Test> tests;
	
	@OneToMany(mappedBy="patient")
	private List<Prescription> prescriptions;
	
	@OneToMany(mappedBy="patient")
	private List<Appointment> appointments;
	
	@ManyToMany
	@JoinTable(name="DOCTORS_PATIENTS", joinColumns=@JoinColumn(name="PATIENT_ID"), inverseJoinColumns=@JoinColumn(name="DOCTOR_ID"))
	private List<Account> doctors;
	
	@ManyToMany
	@JoinTable(name="DOCTORS_PATIENTS", joinColumns=@JoinColumn(name="DOCTOR_ID"), inverseJoinColumns=@JoinColumn(name="PATIENT_ID"))
	private List<Account> patients;	
	
	@ManyToMany
	@JoinTable(name="DOCTORS_SUPPORT", joinColumns=@JoinColumn(name="DOCTOR_ID"), inverseJoinColumns=@JoinColumn(name="SUPPORT_ID"))
	private List<Account> supportStaff;
	
	@ManyToMany
	@JoinTable(name="DOCTORS_SUPPORT", joinColumns=@JoinColumn(name="SUPPORT_ID"), inverseJoinColumns=@JoinColumn(name="DOCTOR_ID") )
	private List<Account> reportTo;

	public List<Account> getReportTo() {
		return reportTo;
	}

	public void setReportTo(List<Account> reportTo) {
		this.reportTo = reportTo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getSpecial() {
		return special;
	}

	public void setSpecial(String special) {
		this.special = special;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public List<ImmunRecord> getImmunRecord() {
		return immunRecord;
	}

	public void setImmunRecord(List<ImmunRecord> immunRecord) {
		this.immunRecord = immunRecord;
	}

	public List<Test> getTests() {
		return tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}

	public List<Prescription> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public List<Account> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Account> doctors) {
		this.doctors = doctors;
	}

	public List<Account> getPatients() {
		return patients;
	}

	public void setPatients(List<Account> patients) {
		this.patients = patients;
	}

	public List<Account> getSupportStaff() {
		return supportStaff;
	}

	public void setSupportStaff(List<Account> supportStaff) {
		this.supportStaff = supportStaff;
	}

	public String getSocialSecurity() {
		return socialSecurity;
	}

	public void setSocialSecurity(String string) {
		this.socialSecurity = string;
	}

	public Character getSex() {
		return sex;
	}

	public void setSex(Character sex) {
		this.sex = sex;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", username=" + username + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", eMail=" + eMail + ", special=" + special + ", address=" + address
				+ ", groupId=" + groupId + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public int compareTo(Account o) {
		int result;
		if( (result = this.firstName.compareToIgnoreCase(o.firstName)) == 0)
			if( (result = this.lastName.compareToIgnoreCase(o.lastName)) == 0)
				result = this.id - o.id;
			
		return result;
	}

}

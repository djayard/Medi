package entities;

import javax.persistence.*;

@Entity
@Table(name="MEDICATIONS")
public class Medication {

	@Id
	@SequenceGenerator(name="medGen", sequenceName="MEDICATION_SEQ")
	@GeneratedValue(generator="medGen", strategy=GenerationType.AUTO)
	@Column(name="MEDICATION_ID")
	private Integer id;
	
	@Column(name="MEDICATION_NAME")
	private String name;
	@Column(name="CHEMICAL_NAME")
	private String chemicalName;
	@Column(name="PRODUCER")
	private String product;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getChemicalName() {
		return chemicalName;
	}
	public void setChemicalName(String chemicalName) {
		this.chemicalName = chemicalName;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	@Override
	public String toString() {
		return "Medication [id=" + id + ", name=" + name + ", chemicalName=" + chemicalName + ", product=" + product
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chemicalName == null) ? 0 : chemicalName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medication other = (Medication) obj;
		if (chemicalName == null) {
			if (other.chemicalName != null)
				return false;
		} else if (!chemicalName.equals(other.chemicalName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}
	
	
	
}

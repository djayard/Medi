package entities;

import javax.persistence.*;

@Entity
@Table(name="PAYMENTS")
public class Payment {

	@Id
	@SequenceGenerator(name="paymentGen", sequenceName="PAYMENT_SEQ")
	@GeneratedValue(generator="paymentGen", strategy=GenerationType.AUTO)
	@Column(name="PAYMENT_ID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="PAYMENT_TYPE")
	private PaymentType type;
	
	@Column(name="BILLED_NAME")
	private String billedName;
	
	@ManyToOne
	@JoinColumn(name="BILLED ADDRESS")
	private Address billingAddress;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PaymentType getType() {
		return type;
	}

	public void setType(PaymentType type) {
		this.type = type;
	}

	public String getBilledName() {
		return billedName;
	}

	public void setBilledName(String billedName) {
		this.billedName = billedName;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", type=" + type + ", billedName=" + billedName + ", billingAddress="
				+ billingAddress + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((billedName == null) ? 0 : billedName.hashCode());
		result = prime * result + ((billingAddress == null) ? 0 : billingAddress.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (billedName == null) {
			if (other.billedName != null)
				return false;
		} else if (!billedName.equals(other.billedName))
			return false;
		if (billingAddress == null) {
			if (other.billingAddress != null)
				return false;
		} else if (!billingAddress.equals(other.billingAddress))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	
}

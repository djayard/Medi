package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="TEST_LINES")
public class TestLine implements Comparable<TestLine> {
	
	@EmbeddedId
	private TestLineId id;
	
	@Column(name="TEST_CODE")
	private String type;
	
	@Column(name="TEST_RESULT")
	private String result;
	
	@Column(name="DATE_COMPLETED")
	private Date completionDate;
	
	
	
	
	public TestLineId getId() {
		return id;
	}




	public void setId(TestLineId id) {
		this.id = id;
	}




	public String getType() {
		return type;
	}




	public void setType(String type) {
		this.type = type;
	}




	public String getResult() {
		return result;
	}




	public void setResult(String result) {
		this.result = result;
	}




	public Date getCompletionDate() {
		return completionDate;
	}




	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}




	@Override
	public String toString() {
		return "TestLine [id=" + id + ", type=" + type + ", result=" + result + ", completionDate=" + completionDate
				+ "]";
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((completionDate == null) ? 0 : completionDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TestLine other = (TestLine) obj;
		if (completionDate == null) {
			if (other.completionDate != null)
				return false;
		} else if (!completionDate.equals(other.completionDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}




	@Embeddable
	public static class TestLineId implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6168685742831999191L;

		@ManyToOne
		@JoinColumn(name="TEST_ID")
		private Test test;
		
		@Column(name="LINE_ID")
		private Integer lineId;

		public Test getTest() {
			return test;
		}

		public void setTest(Test test) {
			this.test = test;
		}

		public Integer getLineId() {
			return lineId;
		}

		public void setLineId(Integer lineId) {
			this.lineId = lineId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((lineId == null) ? 0 : lineId.hashCode());
			result = prime * result + ((test == null) ? 0 : test.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TestLineId other = (TestLineId) obj;
			if (lineId == null) {
				if (other.lineId != null)
					return false;
			} else if (!lineId.equals(other.lineId))
				return false;
			if (test == null) {
				if (other.test != null)
					return false;
			} else if (!test.equals(other.test))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "TestLineId [test=" + test + ", lineId=" + lineId + "]";
		}
		
		
		
	}




	public int compareTo(TestLine o) {
		return this.id.lineId - o.id.lineId;
	}

}

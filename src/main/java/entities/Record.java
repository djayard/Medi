package entities;

import javax.persistence.*;

@Entity
@Table(name="RECORDS")
public class Record {
	@Id
	@SequenceGenerator(name="recordGen", sequenceName="RECORD_SEQ")
	@GeneratedValue(generator="recordGen", strategy=GenerationType.AUTO)
	@Column(name="RECORD_ID")
	private Integer id;
	
	@Column(name="FILENAME")
	private String filename;
	
	@Column(name="TYPE_DESCRIPTION")
	private String type;
	
	@ManyToOne
	@JoinColumn(name="PATIENT_ID")
	private Account patient;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Account getPatient() {
		return patient;
	}

	public void setPatient(Account patient) {
		this.patient = patient;
	}

	@Override
	public String toString() {
		return "Record [id=" + id + ", filename=" + filename + ", type=" + type + ", patient=" + patient + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		return true;
	}

	
	
	

}

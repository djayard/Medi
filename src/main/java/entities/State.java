package entities;

import java.io.FileReader;
import java.util.Scanner;

import javax.persistence.*;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Entity
@Table(name="STATES")
public class State {
	@Id
	@Column(name="STATE_ID")
	@SequenceGenerator(name="stateGen", sequenceName="STATE_SEQ")
	@GeneratedValue(generator="stateGen",strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="STATE_NAME")
	private String stateName;
	
	@Column(name="STATE_ABBRV")
	private String stateAbbrv;
	
	public State() {
		super();
	}

	public State(int id, String stateName, String stateAbbrv) {
		super();
		this.id = id;
		this.stateName = stateName;
		this.stateAbbrv = stateAbbrv;
	}



	public State(String stateName, String stateAbbrv) {
		super();
		this.stateName = stateName;
		this.stateAbbrv = stateAbbrv;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateAbbrv() {
		return stateAbbrv;
	}

	public void setStateAbbrv(String stateAbbrv) {
		this.stateAbbrv = stateAbbrv;
	}

	public static void main(String[] args)
	{
		
		
		Scanner scanner = null;
		try{
			FileReader reader = new FileReader("src/main/resources/states.txt");
			scanner = new Scanner(reader);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		
		final int NUM_STATES = 50;
		State states[] = new State[NUM_STATES];
		
		for(int i = 0; i < NUM_STATES; ++i)
		{
			states[i] = new State();
			String stateName = scanner.next().replace('_', ' ');
			states[i].setStateName(stateName);
			states[i].setStateAbbrv(scanner.next());
		}
		scanner.close();
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("dao-bean.xml");
		SessionFactory factory = (SessionFactory) ctx.getBean("sf");
		
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		for(int i = 0; i < NUM_STATES; ++i)
			session.save(states[i]);
		
		tx.commit();
		
		session.close();
	}

	@Override
	public String toString() {
		return "State [id=" + id + ", stateName=" + stateName + ", stateAbbrv=" + stateAbbrv + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stateAbbrv == null) ? 0 : stateAbbrv.hashCode());
		result = prime * result + ((stateName == null) ? 0 : stateName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (stateAbbrv == null) {
			if (other.stateAbbrv != null)
				return false;
		} else if (!stateAbbrv.equals(other.stateAbbrv))
			return false;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		return true;
	}
	
	
	
	

}

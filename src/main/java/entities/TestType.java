package entities;

import javax.persistence.*;

@Entity
@Table(name="TEST_TYPES")
public class TestType {
	
	@Id
	@SequenceGenerator(name="testTypeGen",sequenceName="TEST_TYPE_SEQ")
	@GeneratedValue(generator="testTypeGen",strategy=GenerationType.AUTO)
	@Column(name="TEST_TYPE_ID")
	private Integer id;
	
	@Column(name="TEST_CODE")
	private String testCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	@Override
	public String toString() {
		return "TestType [id=" + id + ", testCode=" + testCode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((testCode == null) ? 0 : testCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TestType other = (TestType) obj;
		if (testCode == null) {
			if (other.testCode != null)
				return false;
		} else if (!testCode.equals(other.testCode))
			return false;
		return true;
	}
	
	

}

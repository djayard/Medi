package entities;

import javax.persistence.*;

@Entity
@Table(name="USER_ACCOUNT_TYPES")
public class AccountType {
	
	@Id
	@Column(name="TYPE_ID")
	@SequenceGenerator(name="accTypeGen", sequenceName="USER_ACCT_TYPE_SEQ")
	@GeneratedValue(generator="accTypeGen", strategy=GenerationType.AUTO)
	private Integer id;
	
	
	@Column(name="DESCRIPTION")
	private String description;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountType other = (AccountType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	
	
	

}

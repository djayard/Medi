package entities;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.*;
@Entity
@Table(name="APPOINTMENTS")
public class Appointment {

	@Id
	@SequenceGenerator(name="appointGen", sequenceName="APPOINTMENT_SEQ")
	@GeneratedValue(generator="appointGen", strategy=GenerationType.AUTO)
	@Column(name="APPOINTMENT_ID")
	private Integer id;
	
	@Column(name="APPOINTMENT_TIME")
	private Date time;
	
	@ManyToOne
	@JoinColumn(name="PATIENT_ID")
	private Account patient;
	@ManyToOne
	@JoinColumn(name="DOCTOR_ID")
	private Account doctor;
	
	@OneToOne
	@JoinColumn(name="PATIENT_EVAL")
	private PatientEvaluation evaluation;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Account getPatient() {
		return patient;
	}

	public void setPatient(Account patient) {
		this.patient = patient;
	}

	public Account getDoctor() {
		return doctor;
	}

	public void setDoctor(Account doctor) {
		this.doctor = doctor;
	}

	public PatientEvaluation getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(PatientEvaluation evaluation) {
		this.evaluation = evaluation;
	}


	

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", time=" + time + ", patient=" + patient + ", doctor=" + doctor
				+ ", evaluation=" + evaluation + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doctor == null) ? 0 : doctor.hashCode());
		result = prime * result + ((evaluation == null) ? 0 : evaluation.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (doctor == null) {
			if (other.doctor != null)
				return false;
		} else if (!doctor.equals(other.doctor))
			return false;
		if (evaluation == null) {
			if (other.evaluation != null)
				return false;
		} else if (!evaluation.equals(other.evaluation))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	
	
	
}

package entities;

import javax.persistence.*;

@Entity
@Table(name="PAYMENT_TYPES")
public class PaymentType {
	
	@Id
	@SequenceGenerator(name="payTypeGen", sequenceName="PAYMENT_TYPE_SEQ")
	@GeneratedValue(generator="payTypeGen", strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="Description")
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PaymentType [id=" + id + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentType other = (PaymentType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	
	

}

package entities;

import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name="IMMUNIZATION_RECORDS")
public class ImmunRecord {
	
	@Id
	@SequenceGenerator(name="immuGen", sequenceName="IMMU_RECORD_SEQ")
	@GeneratedValue(generator="immuGen", strategy=GenerationType.AUTO)
	@Column(name="RECORD_ID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="PATIENT_ID")
	private Account patient;
	@ManyToOne
	@JoinColumn(name="TREATER")
	private Account treater;
	@Column(name="TREATMENT_DATE")
	private Date treatmentDate;
	@ManyToOne
	@JoinColumn(name="TREATMENT_TYPE")
	private ImmunType type;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Account getPatient() {
		return patient;
	}
	public void setPatient(Account patient) {
		this.patient = patient;
	}
	public Account getTreater() {
		return treater;
	}
	public void setTreater(Account treater) {
		this.treater = treater;
	}
	public Date getTreatmentDate() {
		return treatmentDate;
	}
	public void setTreatmentDate(Date treatmentDate) {
		this.treatmentDate = treatmentDate;
	}
	public ImmunType getType() {
		return type;
	}
	public void setType(ImmunType type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "ImmunRecord [id=" + id + ", patient=" + patient + ", treater=" + treater + ", treatmentDate="
				+ treatmentDate + ", type=" + type + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + ((treater == null) ? 0 : treater.hashCode());
		result = prime * result + ((treatmentDate == null) ? 0 : treatmentDate.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImmunRecord other = (ImmunRecord) obj;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (treater == null) {
			if (other.treater != null)
				return false;
		} else if (!treater.equals(other.treater))
			return false;
		if (treatmentDate == null) {
			if (other.treatmentDate != null)
				return false;
		} else if (!treatmentDate.equals(other.treatmentDate))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	

}

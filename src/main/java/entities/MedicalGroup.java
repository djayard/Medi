package entities;

import javax.persistence.*;

@Entity
@Table(name="MEDICAL_GROUPS")
public class MedicalGroup {
	
	@Id
	@Column(name="MEDICAL_GROUP_ID")
	@SequenceGenerator(name="groupGen",sequenceName="MED_GROUP_SEQ")
	@GeneratedValue(generator="groupGen",strategy=GenerationType.AUTO)
	private Integer id;
	
	
	@Column(name="GROUP_NAME")
	private String name;
	
	@OneToOne
	@JoinColumn(name="MAIN_ADDRESS")
	private Address address;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "MedicalGroup [id=" + id + ", name=" + name + ", address=" + address + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedicalGroup other = (MedicalGroup) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

}

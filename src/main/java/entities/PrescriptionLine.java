package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="PRESCRIPTION_LINES")
public class PrescriptionLine {
	@EmbeddedId
	private PrescriptionLineId id;
	
	@Column(name="QUANTITY")
	private double quantity;
	@Column(name="UNITS")
	private String units;
	@Column(name="REFILLS")
	private Integer refills;
	@ManyToOne
	@JoinColumn(name="MEDICATION")
	private Medication medication;
	public PrescriptionLineId getId() {
		return id;
	}
	public void setId(PrescriptionLineId id) {
		this.id = id;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public Integer getRefills() {
		return refills;
	}
	public void setRefills(Integer refills) {
		this.refills = refills;
	}
	public Medication getMedication() {
		return medication;
	}
	public void setMedication(Medication medication) {
		this.medication = medication;
	}
	@Override
	public String toString() {
		return "PrescriptionLine [id=" + id + ", quantity=" + quantity + ", units=" + units + ", refills=" + refills
				+ ", medication=" + medication + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((medication == null) ? 0 : medication.hashCode());
		long temp;
		temp = Double.doubleToLongBits(quantity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((refills == null) ? 0 : refills.hashCode());
		result = prime * result + ((units == null) ? 0 : units.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrescriptionLine other = (PrescriptionLine) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (medication == null) {
			if (other.medication != null)
				return false;
		} else if (!medication.equals(other.medication))
			return false;
		if (Double.doubleToLongBits(quantity) != Double.doubleToLongBits(other.quantity))
			return false;
		if (refills == null) {
			if (other.refills != null)
				return false;
		} else if (!refills.equals(other.refills))
			return false;
		if (units == null) {
			if (other.units != null)
				return false;
		} else if (!units.equals(other.units))
			return false;
		return true;
	}
	
	
	@Embeddable
	public static class PrescriptionLineId implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 195950559950718421L;

		@ManyToOne
		@JoinColumn(name="PRESCRIPTION_ID")
		private Prescription prescript;
		
		@Column(name="LINED_ID")
		private Integer lineId;

		public Prescription getPrescript() {
			return prescript;
		}

		public void setPrescript(Prescription prescript) {
			this.prescript = prescript;
		}

		public Integer getLineId() {
			return lineId;
		}

		public void setLineId(Integer lineId) {
			this.lineId = lineId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((lineId == null) ? 0 : lineId.hashCode());
			result = prime * result + ((prescript == null) ? 0 : prescript.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PrescriptionLineId other = (PrescriptionLineId) obj;
			if (lineId == null) {
				if (other.lineId != null)
					return false;
			} else if (!lineId.equals(other.lineId))
				return false;
			if (prescript == null) {
				if (other.prescript != null)
					return false;
			} else if (!prescript.equals(other.prescript))
				return false;
			return true;
		}
		
		
	}
	
	
}

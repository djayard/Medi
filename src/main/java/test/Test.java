package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Test {

	public static void main(String[] args)
	{
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		System.out.println(encoder.encode("nero"));
		System.out.println(encoder.encode("dum1"));
		System.out.println(encoder.encode("zaros"));
		System.out.println(encoder.encode("sux"));
		System.out.println(encoder.encode("whatwhat"));
		System.out.println(encoder.encode("helloNurse"));
		System.out.println(encoder.encode("123456789"));
		System.out.println(encoder.encode("doctor1"));
		System.out.println(encoder.encode("doctor2"));
	}
	
}

package medi.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;


public class HomeControllerTest {

	@Test
	public void testHomePage() throws Exception{
		PatientController controller = new PatientController();
		MockMvc mockMvc = standaloneSetup(controller).build();
		mockMvc.perform(get("/homePath")).andExpect(view().name("home"));
	}
	
}
